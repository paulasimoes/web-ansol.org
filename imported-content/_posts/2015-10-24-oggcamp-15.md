---
categories:
- free culture
- cultura livre
- software livre
metadata:
  event_location:
  - event_location_value: Liverpool, UK
  event_site:
  - event_site_url: http://oggcamp.org/
    event_site_title: OggCamp 15
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2015-10-31 00:00:00.000000000 +00:00
    event_start_value2: 2015-11-01 00:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 110
  - tags_tid: 111
  - tags_tid: 41
  node_id: 356
layout: evento
title: OggCamp 15
created: 1445726272
date: 2015-10-24
---
<p>OggCamp 15 is once again taking place at the&nbsp;<strong>LJMU John Lennon Art &amp; Design Building</strong>&nbsp;in Liverpool. Our venue is right next to the Metropolitan Cathedral (the huge building that looks like the cooling tower from the&nbsp;<em>Aliens&nbsp;</em>movie) and can be reached within ten walking minutes from&nbsp;<strong>Liverpool Lime Street Station</strong>which is the end point for many national railway connections.</p><p>The full postal address is: LJMU Art &amp; Design Academy, Duckinfield Street, Liverpool, L3 5RB, United Kingdom</p><p>There will be WiFi throughout the venue. If you have an eduroam account, you are welcome to use that, otherwise a guest log-in will be provided.&nbsp; You may find that it struggles at the start of the day when everyone tries to log in and propose a talk at once – please be patient, it will recover.</p><p>Below is a map with highlights of the important locations for OggCamp 15. A&nbsp;<a href="https://a.tiles.mapbox.com/v4/marxjohnson.npm9d9i0/page.html?access_token=pk.eyJ1IjoibWFyeGpvaG5zb24iLCJhIjoiZmpUMUFfQSJ9.b2oRUMyfC8P-FbgN8sUoBg#14/53.4045/-2.9731">bigger map</a>&nbsp;is also available.</p>
