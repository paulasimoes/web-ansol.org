---
categories:
- ttip
metadata:
  event_location:
  - event_location_value: Lisboa
  event_site:
  - event_site_url: https://www.nao-ao-ttip.pt/jantar-de-apoio-debate-da-plataforma-nao-a-parceria-transatlantica-ttip-612/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2014-12-06 20:00:00.000000000 +00:00
    event_start_value2: 2014-12-06 20:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 54
  node_id: 251
layout: evento
title: Jantar de Apoio + Debate da Plataforma Não à Parceria Transatlântica (TTIP)
created: 1417608089
date: 2014-12-03
---
<p><img src="https://www.nao-ao-ttip.pt/wp-content/uploads/2014/12/jantar-debate-1024x378.jpg" width="1024" height="378"></p>
