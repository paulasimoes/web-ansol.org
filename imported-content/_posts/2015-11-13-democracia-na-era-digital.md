---
categories:
- markus beckedahl
- netzpolitik.org
- marisa matias
- democracia
- era digital
metadata:
  event_location:
  - event_location_value: Goethe-Institut em Lisboa
  event_site:
  - event_site_url: http://www.goethe.de/ins/pt/lis/ver/pt14859387v.htm
    event_site_title: Democracia na era digital
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2015-11-30 19:00:00.000000000 +00:00
    event_start_value2: 2015-11-30 19:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 153
  - tags_tid: 154
  - tags_tid: 155
  - tags_tid: 156
  - tags_tid: 157
  node_id: 381
layout: evento
title: Democracia na era digital
created: 1447438272
date: 2015-11-13
---
<h3 class="veranstaltungsuschrift">Democracia na era digital</h3><h4 class="verunteruschrift">Markus Beckedahl e Marisa Matias em conversa</h4><div class="veranstaltungsintro">Palestra e debate<br>30.11.2015, 19h00<br>Goethe-Institut em Lisboa<br>Auditório<br>Campo dos Mártires da Pátria, 37<br>Alemão e Português, com tradução simultânea<br>Entrada livre<br>218824511<br><img src="http://www.goethe.de/bilder3/symbole/mail.gif" alt="" width="16" height="11" border="0"><a href="mailto:biblioteca@lissabon.goethe.org">biblioteca@lissabon.goethe.org</a></div><div class="veranstaltungstext"><div class="bildBox"><div class="bild"><img src="http://www.goethe.de/mmo/priv/14941083-STANDARD.jpg" alt="" title="" align="left" class="normalgrafik"></div></div><br clear="all"><p>No dia&nbsp;<strong>30 de Novembro</strong>, às&nbsp;<strong>19h00</strong>, terá lugar, no Goethe-Institut em Lisboa, a primeira palestra da série de conferências "Viver na sociedade digital".&nbsp;<strong>Markus Beckedahl</strong>, redactor-chefe do blogue netzpolitik.org, irá conversar com&nbsp;<strong>Marisa Matias</strong>, Deputada do Parlamento Europeu pelo Bloco de Esquerda, sobre o tema<strong>Democracia na era digital</strong>. A sessão será moderada por&nbsp;<strong>Nicolau Santos</strong>.</p><p><strong>Democracia na era digital</strong><br>Cada vez mais, a era digital transforma as nossas vidas. Países e empresas adquirem acesso quase ilimitado às redes sociais, aos dados dos nossos telemóveis e ao nosso correio electrónico, vigiando constantemente a vida dos cidadãos. Graças ao Wikileaks e a Edward Snowden podemos ter uma ideia da dimensão dessa vigilância. Mas teremos também ideia do perigo que ela representa para as nossas democracias ocidentais?</p><br clear="all"><img src="http://www.goethe.de/mmo/priv/14859396-STANDARD.jpg" alt="(c) Fiona Krakenbuerger" title="(c) Fiona Krakenbuerger" align="left" class="normalgrafik"><strong>Markus Beckedahl</strong>&nbsp;é jornalista e redactor-chefe do blogue netzpolitik.org. É também organizador da re:publica, a maior conferência sobre internet e sociedade na Europa, membro do conselho de comunicação social da Medienanstalt Berlin-Brandenburg e ex-membro da comissão de inquérito Internet e Sociedade Digital do parlamento alemão.<p>&nbsp;</p><br><br><br clear="all"><img src="http://www.goethe.de/mmo/priv/14917930-STANDARD.jpg" alt="(c) " title="" align="left" class="normalgrafik"><strong>Marisa Matias</strong>&nbsp;é deputada do Parlamento Europeu pelo Bloco de Esquerda. Doutorada em Sociologia pela Faculdade de Letras da Universidade de Coimbra, é vice-presidente da Comissão especial TAXE (Decisões fiscais antecipadas e outras medidas de natureza ou efeitos similares), coordenadora para o grupo parlamentar da Comissão ECON (Assuntos Económicos e Monetários) e co-presidente do Intergrupo Bens Comuns, entre outros cargos.<p>&nbsp;</p><br><table style="width: 100%;" border="0" bgcolor="gainsboro"><tbody><tr><td>Com o objectivo de pensar e debater as condições de vida na era digital, a série de conferências&nbsp;<strong>Viver na sociedade digital</strong>&nbsp;reúne intervenientes das áreas da política, das artes, da cultura e da internet. A questão é comum a todos: como articular a transformação digital em busca de um futuro melhor?</td></tr></tbody></table></div>
