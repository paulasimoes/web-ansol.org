---
excerpt: "<p>Encontro T&eacute;cnico da comunidade Porto Linux que receber&aacute;
  Rui Lapa e Carlos Ferreira numa apresenta&ccedil;&atilde;o que promete ser elucidativa
  acerca da evolu&ccedil;&atilde;o e das solu&ccedil;&otilde;es desenvolvidas e adotadas
  na migra&ccedil;&atilde;o para Software livre levada a cabo pela Tranquilidade.
  No Audit&oacute;rio Magno do Instituto Superior de Enenharia do Porto (ISEP) no
  pr&oacute;ximo dia 13 de Outubro, pelas 15h.</p>\r\n"
categories: []
metadata:
  event_location:
  - event_location_value: Auditório Magno do Instituto Superior de Enenharia do Porto
      (ISEP)
  event_start:
  - event_start_value: 2012-10-13 14:00:00.000000000 +01:00
    event_start_value2: 2012-10-13 17:00:00.000000000 +01:00
  node_id: 91
layout: evento
title: A Adoção de Software Livre pela Tranquilidade
created: 1349213022
date: 2012-10-02
---
<p>As novidades recentes vindas a p&uacute;blico acerca a ado&ccedil;&atilde;o de Software Livre por parte da Companhia de Seguros Tranquilidade, S.A. (Grupo Esp&iacute;rito Santo) conduziram a um interesse da comunidade Porto Linux em torno deste processo e o boca-em-boca levou-nos ao contacto com os&nbsp; respons&aacute;veis pelo processo levado a cabo.<br />
	<br />
	&Eacute; com todo o prazer que anuncio que, com o tema ainda a quente nos &oacute;rg&atilde;os de comunica&ccedil;&atilde;o, o Porto Linux receber&aacute; Rui Lapa e Carlos Ferreira numa apresenta&ccedil;&atilde;o que promete ser elucidativa acerca da evolu&ccedil;&atilde;o e das solu&ccedil;&otilde;es desenvolvidas e adotadas.<br />
	<br />
	O evento decorrer&aacute; no Audit&oacute;rio Magno do Instituto Superior de Enenharia do Porto (ISEP) no pr&oacute;ximo dia 13 de Outubro, pelas 15h.<br />
	<br />
	A comunidade Porto Linux agradece a disponibilidade de Rui Lapa e de Carlos Ferreira e todo o apoio log&iacute;stico fornecido pelo Departamento de Engenharia Inform&aacute;tica (DEI) do ISEP e, claro, do pr&oacute;prio ISEP.<br />
	<br />
	A entrada, como sempre, &eacute; gratuita, limitada &agrave; lota&ccedil;&atilde;o do espa&ccedil;o (que, neste caso, &eacute; MUITO grande!).</p>
