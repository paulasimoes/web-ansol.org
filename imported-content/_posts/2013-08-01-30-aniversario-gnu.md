---
categories: []
metadata:
  event_site:
  - event_site_url: https://www.fsf.org/events/happy-30th-birthday-gnu
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2013-09-26 23:00:00.000000000 +01:00
    event_start_value2: 2013-09-26 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 203
layout: evento
title: 30º aniversário GNU
created: 1375350589
date: 2013-08-01
---
<p>A ANSOL irá celebrar os 30 anos do GNU durante o evento do Software Freedom Day.</p><p>Mais detalhes em https://ansol.org/node/130 .</p>
