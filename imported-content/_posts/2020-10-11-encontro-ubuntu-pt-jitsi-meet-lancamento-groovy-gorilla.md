---
categories: []
metadata:
  event_location:
  - event_location_value: https://meet.ubcasts.org/UbuntuPTOutubro
  event_start:
  - event_start_value: 2020-10-22 20:00:00.000000000 +01:00
    event_start_value2: 2020-10-22 20:00:00.000000000 +01:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 757
layout: evento
title: Encontro Ubuntu-pt @ Jitsi Meet // Lançamento Groovy Gorilla
created: 1602430207
date: 2020-10-11
---
<p>2 vezes por ano (em Abril e Outubro), numa quinta-feira, a comunidade Ubuntu Portugal reúne-se para assinalar/festejar o lançamento de uma nova versão Ubuntu.<br><br>Para celebrar o lançamento da versão 20.10 do Ubuntu, que procura trazer o melhor do Software Livre ao público em geral.<br><br>Vem fazer o upgrade connosco!<br><br>O encontro irá decorrer pelas 21h do dia 22 de Outubro e será feito via Jitsi (<a href="https://meet.ubcasts.org/UbuntuPTOutubro" target="__blank" title="https://meet.ubcasts.org/UbuntuPTOutubro" class="link">https://meet.ubcasts.org/UbuntuPTOutubro</a>).<br><br>Vemo-nos lá e traz um amigo! Todos são bem-vindos!</p>
