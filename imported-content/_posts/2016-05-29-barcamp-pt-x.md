---
categories: []
metadata:
  event_location:
  - event_location_value: Coimbra
  event_site:
  - event_site_url: http://barcamppt.org/index.php/BarCamp_PT_X
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2016-09-02 23:00:00.000000000 +01:00
    event_start_value2: 2016-09-02 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 425
layout: evento
title: Barcamp PT X
created: 1464545774
date: 2016-05-29
---
<p>Exactamente 10 anos depois do primeiro, o BarCamp PT volta a acontecer em Coimbra.</p>
