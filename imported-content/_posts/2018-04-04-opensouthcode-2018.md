---
categories:
- open source
- free software
- linux
- espanha
metadata:
  event_location:
  - event_location_value: La Térmica, Málaga, Espanha
  event_site:
  - event_site_url: https://www.opensouthcode.org/conferences/opensouthcode2018
    event_site_title: Open South Code2018
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-05-31 23:00:00.000000000 +01:00
    event_start_value2: 2018-06-01 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 127
  - tags_tid: 122
  - tags_tid: 242
  - tags_tid: 269
  node_id: 583
layout: evento
title: Opensouthcode 2018
created: 1522867217
date: 2018-04-04
---
<h3 class="text-center"><strong>Opensouthcode</strong> es un evento para promocionar y dar a conocer las tecnologías abiertas: software/hardware libre y opensource. El evento es gratuito.</h3>
