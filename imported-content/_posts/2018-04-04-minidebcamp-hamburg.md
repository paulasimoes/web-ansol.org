---
categories:
- debian
- camp
- minidebconf
- minidebcamp
metadata:
  event_location:
  - event_location_value: Victoria Kaserne, Hamburgo, Alemanhas
  event_site:
  - event_site_url: https://wiki.debian.org/DebianEvents/de/2018/MiniDebConfHamburg
    event_site_title: MiniDebCamp
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-05-15 23:00:00.000000000 +01:00
    event_start_value2: 2018-05-17 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 66
  - tags_tid: 254
  - tags_tid: 255
  - tags_tid: 256
  node_id: 574
layout: evento
title: MiniDebCamp Hamburg
created: 1522856731
date: 2018-04-04
---
<p>Um MiniDebCamp é um período de tempo em que os developers e voluntários Debian se organizam para trabalhar em conjunto no projecto Debian Linux. Normalmente estes campos precedem um encontro Debconf.</p>
