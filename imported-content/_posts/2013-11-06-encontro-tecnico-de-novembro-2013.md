---
categories:
- '2013'
metadata:
  event_location:
  - event_location_value: " Xarevision  Rua Ciríaco Cardoso, 265 - i, Porto"
  event_site:
  - event_site_url: http://portolinux.org/doku.php?id=encontrostecnicos:nov13
    event_site_title: Página oficial do encontro
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2013-11-09 15:00:00.000000000 +00:00
    event_start_value2: 2013-11-09 20:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 32
  node_id: 212
layout: evento
title: Encontro técnico de Novembro 2013
created: 1383779237
date: 2013-11-06
---
<div class="level1"><p>Vai-se realizar no dia <strong>9 de Novembro de 2013</strong> pelas <strong>15h</strong> um encontro técnico com o seguinte alinhamento:</p><ul><li class="level2"><div class="li"><a href="http://fluentd.org/"><strong>FluentD</strong></a> - análise de logs , primeiro contacto, 1a parte por César Araújo e 2a parte por Tomás Lima</div></li><li class="level2"><div class="li"><strong>Magusto</strong> (improvisado)</div></li><li class="level2"><div class="li"><a href="https://www.docker.io/"><strong>Docker</strong></a> – Containers para seres urbanos, por José Pinheiro Neta</div></li></ul></div>
