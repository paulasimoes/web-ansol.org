---
categories: []
metadata:
  event_location:
  - event_location_value: 'Mob - Espaço Associativo :: Rua dos Anjos 12F, Lisboa'
  event_site:
  - event_site_url: https://www.facebook.com/events/848230828555415/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2014-11-25 21:30:00.000000000 +00:00
    event_start_value2: 2014-11-25 21:30:00.000000000 +00:00
  slide:
  - slide_value: 0
  node_id: 246
layout: evento
title: TTIP - O que estão a negociar em segredo?
created: 1416237529
date: 2014-11-17
---
<p><span class="fsl">Conversa sobre a Parceria Transatlântica de Comércio e Investimento</span></p><p><span class="fsl">Debate com:<br> <br> Carla Graça - Quercus<br> Conceição Alpiarça - Plataforma Não ao TTIP<br> Luís Bernardo - Conselho Editorial <a href="https://www.facebook.com/mondediplo.pt" data-hovercard="/ajax/hovercard/page.php?id=89119479518">Le Monde Diplomatique - ed. portuguesa</a><br> Sara Simões - Associação de Combate à Precariedade - Precários Inflexíveis<br> <br> A Parceria Transatlântica de Comércio e Investimento (TTIP) é um tratado abrangente de livre comércio e investimento que está a ser negociado - em segredo - entre a União Europeia e os Estados Unidos. Desta negociação secreta têm sido divulgados alguns documentos que nos mostram que os impactos da implementação de uma parceria deste tipo, nos moldes em que está a ser feita a discussão, terá efeitos desastrosos em todos os aspetos da nossa organização como sociedade. <br> <br> Queremos com este debate pôr a descoberto o que estão a negociar em segredo e perceber quais os impactos que a aprovação de uma carta deste tipo implicaria nos campos ambiental, económico e laboral. Além disso queremos ainda fazer um ponto de situação do movimento social que está a crescer por toda a Europa contra o TTIP.</span></p>
