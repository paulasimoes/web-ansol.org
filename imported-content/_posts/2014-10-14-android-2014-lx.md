---
categories: []
metadata:
  event_location:
  - event_location_value: Hotel Florida, Lisboa
  event_site:
  - event_site_url: http://www.meetup.com/Android-LX/events/210931492/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2014-10-14 18:00:00.000000000 +01:00
    event_start_value2: 2014-10-14 18:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 236
layout: evento
title: Android 2014 LX
created: 1413297139
date: 2014-10-14
---
<p>19h - Início do Evento<br> - Apresentação sobre "Android UI e UX"<br> - Apresentação sobre "Don't repeat yourself"</p><p>21h - Pausa para jantar e convívio patrocinados pelo evento</p><p>22h - Apresentação sobre "Desenvolver para Chromecast"</p>
