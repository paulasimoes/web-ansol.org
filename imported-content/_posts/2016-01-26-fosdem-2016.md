---
categories:
- foss
metadata:
  event_location:
  - event_location_value: Bruxelas, Bélgica
  event_site:
  - event_site_url: https://fosdem.org/2016
    event_site_title: Fosdem'16
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2016-01-30 00:00:00.000000000 +00:00
    event_start_value2: 2016-01-31 00:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 160
  node_id: 398
layout: evento
title: FOSDEM 2016
created: 1453822481
date: 2016-01-26
---
<h2>FOSDEM is a free event for software developers to meet, share ideas and collaborate.</h2><p>Every year, thousands of developers of free and open source software from all over the world gather at the event in Brussels.</p><p class="btn-news"><a href="https://fosdem.org/2016/practical/">No registration necessary.</a></p>
