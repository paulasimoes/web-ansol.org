---
categories: []
metadata:
  event_location:
  - event_location_value: Lisboa
  event_site:
  - event_site_url: https://www.nao-ao-ttip.pt/conferencia-verdade-ou-consequencia-impactos-do-tratado-transatlantico-de-comercio-e-investimento-ue-eua/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2014-12-21 17:00:00.000000000 +00:00
    event_start_value2: 2014-12-21 17:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  node_id: 258
layout: evento
title: 'CONFERÊNCIA > Verdade ou consequência: impactos do Tratado Transatlântico
  de Comércio e Investimento UE-EUA'
created: 1418676252
date: 2014-12-15
---
<p class="post-date">15-12-2014</p><div class="post-content"><h3>Domingo | 21 de Dezembro | 17h00 |<br> Hotel Borges<br> Rua Garrett 108, Lisboa.</h3><p>&nbsp;</p><p>Associando-se ao movimento europeu contra o acordo de Parceria Transatlântica de Comércio e Investimento (TTIP), a Candidatura Cidadã Tempo de Avançar convida todos os interessados a participar numa conferência no próximo dia 21 de Dezembro, pelas 17 horas, no Hotel Borges Chiado (Rua Garrett, 108 Baixa-Chiado, Lisboa). Este encontro tem como objectivo aprofundar o debate público sobre este instrumento, será moderado por Isabel do Carmo (Associação Fórum Manifesto) e contará com os contributos de Nuno Teles (Universidade de Coimbra), Sara Simões (Precários Inflexíveis), Carlos Teixeira (LIVRE) e Pedro Santos (O que esconde o TTIP?), bem como a participação especial de Monica FRASSONI , antiga eurodeputada e Co-Presidente dos European Greens. Estarão igualmente presentes representantes de um conjunto de organizações da sociedade civil que se têm destacado na denúncia dos impactos do TTIP e na crítica à ausência de um verdadeiro escrutínio democrático em torno deste acordo.</p></div>
