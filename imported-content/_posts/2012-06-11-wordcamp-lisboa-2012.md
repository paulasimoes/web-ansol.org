---
categories: []
metadata:
  event_location:
  - event_location_value: Lisboa
  event_site:
  - event_site_url: http://2012.lisboa.wordcamp.org/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2012-09-28 23:00:00.000000000 +01:00
    event_start_value2: 2012-09-29 23:00:00.000000000 +01:00
  node_id: 81
layout: evento
title: WordCamp Lisboa 2012
created: 1339425516
date: 2012-06-11
---
<p>O Word&shy;Camp &eacute; <a href="http://central.wordcamp.org/what-to-expect/" target="_blank">um evento global e sem fins lucrativos</a>. &Eacute; uma con&shy;fer&shy;&ecirc;n&shy;cia infor&shy;mal, orga&shy;ni&shy;zada local&shy;mente, ded&shy;i&shy;cada a tudo o que est&aacute; rela&shy;cionado com o Word&shy;Press, o melhor e mais usado sistema de gest&atilde;o de conte&uacute;dos do mundo.</p>
<p>Dirige-se tanto a quem est&aacute; a come&ccedil;ar, como a uti&shy;lizadores exper&shy;i&shy;entes ou profis&shy;sion&shy;ais, que queiram apro&shy;fun&shy;dar os seus con&shy;hec&shy;i&shy;men&shy;tos e encontrar-se &ldquo;ao vivo&rdquo;. Conta normalmente com um painel de refer&shy;&ecirc;n&shy;cia de par&shy;tic&shy;i&shy;pantes e oradores nacionais e inter&shy;na&shy;cionais.</p>
