---
categories: []
metadata:
  event_location:
  - event_location_value: ULB Solboch, Bruxelas, Bélgica
  event_site:
  - event_site_url: https://fosdem.org/2014/
    event_site_title: FOSDEM
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2014-01-31 18:00:00.000000000 +00:00
    event_start_value2: 2014-02-02 17:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  node_id: 213
layout: evento
title: FOSDEM 2014
created: 1390173993
date: 2014-01-19
---
<p>A maior conferência europeia anual de Software Livre feita por comunidades para as comunidades, habitualmente começa com o "<em>beer event</em>" no Delirium Café, Sexta-feira ao fim do dia, como forma de aquecer o ambiente antes dos dois dias intensos de conferências.</p>
