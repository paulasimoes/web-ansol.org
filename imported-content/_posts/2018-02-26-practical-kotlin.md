---
categories: []
metadata:
  event_location:
  - event_location_value: Coimbra
  event_site:
  - event_site_url: https://www.meetup.com/Coimbra-JUG/events/247383133/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-03-06 00:00:00.000000000 +00:00
    event_start_value2: 2018-03-06 00:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  node_id: 545
layout: evento
title: Practical Kotlin
created: 1519675776
date: 2018-02-26
---
<p>No 21º encontro do Coimbra JUG, teremos o prazer de contar com mais um orador internacional: o Artur Czopek (<a href="https://twitter.com/ACzopek32" target="__blank" title="https://twitter.com/ACzopek32" class="link">https://twitter.com/ACzopek32</a>). O Artur vai apresentar uma sessão sobre Kotlin, uma linguagem da JVM que começa a ganhar grande popularidade e que passou a ser totalmente suportada no Android.</p>
