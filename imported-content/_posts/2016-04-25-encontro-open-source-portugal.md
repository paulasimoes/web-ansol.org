---
categories: []
metadata:
  event_location:
  - event_location_value: IAPMEI
  event_site:
  - event_site_url: http://ospt.artica.cc/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2016-05-29 23:00:00.000000000 +01:00
    event_start_value2: 2016-05-29 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 416
layout: evento
title: Encontro Open Source Portugal
created: 1461605402
date: 2016-04-25
---
<p><img src="http://artica.cc/assets/images/2016-04-21-ospt.jpg" alt="Poster" width="800" height="1131"></p>
