---
categories:
- linux
- free software
- open source
- croacia
metadata:
  event_location:
  - event_location_value: FER, Zagreb, Croatia
  event_site:
  - event_site_url: https://2018.dorscluc.org/conference/
    event_site_title: DORS/CLUC
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-04-18 23:00:00.000000000 +01:00
    event_start_value2: 2018-04-19 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 242
  - tags_tid: 122
  - tags_tid: 127
  - tags_tid: 245
  node_id: 568
layout: evento
title: DORS/CLUC 2018
created: 1522848054
date: 2018-04-04
---
<p>DORS/CLUC - Dani otvorenih računarskih sustava / Croatian Linux Users' Conference - is the oldest and biggest regional conference that covers free and open source software, open standards, and the Linux operating system. For the last 25 years, DORS/CLUC has brought together many prominent individuals and companies from the free and open source software community.<br><br>The conference is coordinated by HrOpen and HULK, two non-profit organizations that tirelessly work to promote open technologies and encourage their application in everyday life.<br><br>This year's conference will take place from April 19 to 20 at the Faculty of Electrical Engineering and Computing in Zagreb, Croatia. Once again, an interesting crowd will gather to attend talks about awesome free software topics.<br><br>During the three days of talks, workshops, and fun, DORS/CLUC becomes a place where students, FOSS enthusiasts, tinkerers, developers, hackers, freelancers, companies and the public sector representatives meet to learn, network, do business and plan projects together - all with focus on free software and Linux. </p><p>DORS/CLUC has traditionally been divided into two parts: keynotes/talks and workshops.<br><br>Keynotes and talks generally cover business topics, the latest experiences with FLOSS implementation and migration, and presentations of new tools and technologies, along with explanations on how to use them.<br><br>Workshops are mostly tech-oriented, and provide the attendees with an opportunity to learn how to use a particular technology, as well as how to troubleshoot and solve problems with free and open source software and hardware.<br><br>There is something for everyone at DORS/CLUC. Whether you are a beginner or an experienced member of the FOSS community, we believe that three days full of interesting talks, networking, and knowledge exchange will count as time well-spent.<br><br></p>
