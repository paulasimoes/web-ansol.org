---
categories: []
metadata:
  event_location:
  - event_location_value: Coimbra
  event_site:
  - event_site_url: http://wm2016.wireless-meeting.com/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2016-05-30 23:00:00.000000000 +01:00
    event_start_value2: 2016-05-30 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 424
layout: evento
title: Wireless Meeting 2016
created: 1464545462
date: 2016-05-29
---
<p><span style="font-size: 14px;"><span style="font-weight: normal; margin: 0px; color: #000000;">É ja no próximo dia 31 que se irá&nbsp;realizar o maior encontro de tecnologia wireless do país, o&nbsp;Wireless Meeting 2016, no Convento de S. Francisco, em Coimbra.<br><br>O Wireless Meeting, é um evento que se tornou uma referência no mundo das tecnologias Wireless, posicionando-se como um marco no calendário nacional dos eventos tecnológicos.</span></span></p>
