---
metadata:
  node_id: 33
layout: page
title: Regulamento Interno da ANSOL
created: 1334480186
date: 2012-04-15
aliases:
- "/node/33/"
- "/page/33/"
- "/regulamento_interno/"
---
O regulamento interno da ANSOL foi revisto pela última vez em 2022.

<style>
ol > li > ol { list-style-type: lower-alpha; }
ol.alpha { list-style-type: lower-alpha; }
</style>

## CAPÍTULO I

(Dos Sócios)

### Artigo Primeiro

(Categorias de Sócios)

1. Existem as seguintes categorias de sócios:
   1. Sócios Honorários;
   1. Sócios Ordinários.
1. A nomeação de Sócios Honorários será realizada pela Assembleia Geral por
   proposta da Direcção em exercício ou por, pelo menos, um quinto dos sócios
   no pleno gozo dos seus direitos.

### Artigo Segundo

(Jóia e Quotas)

1. A inscrição na Associação obriga ao pagamento antecipado de uma Jóia de
   inscrição e de uma Quota anual.</li>
1. Os Sócios Honorários estão isentos do pagamento de Jóia e Quotas.</li>
1. A alteração do valor da Jóia de inscrição e da Quota anual será decidida
   pela Assembleia Geral por proposta da Direcção em exercício.</li>
1. A Assembleia Geral pode estabelecer uma Quota de valor reduzido para
   estudantes, reformados e desempregados.</li>
1. Os Sócios deverão regularizar as quotas no máximo até 30 dias após a
   caducidade da quotização anterior.
   1. A Direcção deverá informar os sócios, por correio electrónico, quando se
      aproximar a data em que caduca a sua quotização;
   1. A Direcção poderá suspender um sócio que, após decorridos os 30 dias, não
      tenha regularizado a situação;
   1. Se a irregularidade se mantiver por mais de 90 dias poderá a Direcção
      excluir o sócio, devendo comunicar-lhe por escrito esta decisão e
      notificar o Conselho Fiscal.
1. Um sócio pode solicitar à Direcção a sua auto-suspensão ou auto-exclusão.
   1. O tempo de quotização restante no momento do pedido de auto-suspensão
      será reposto na altura que o sócio solicitar o cancelamento da suspensão;
   1. A auto-exclusão não dá direito ao reembolso da Jóia nem de Quotas.

### Artigo Terceiro

(Admissão de Sócios)

1. A admissão dos associados depende cumulativamente de:
   1. Preenchimento correcto do Formulário de Candidatura que se encontra no
      sítio (site) oficial da Associação;
   1. Aprovação pela Direcção;
   1. Pagamento da Jóia de inscrição;
   1. Pagamento das Quotas relativas ao primeiro ano, num prazo de 90 dias após
      a sua aprovação pela Direcção.
1. Após recepção e análise do Formulário de Candidatura, deve a Direcção
   comunicar ao candidato a sua aceitação ou rejeição.
   1. A deliberação da Direcção sobre o requerimento de admissão é susceptível
      de recurso para a primeira Assembleia Geral subsequente;
   1. Têm legitimidade para recorrer os sócios da Associação e o candidato,
      podendo este assistir a essa assembleia geral e participar na discussão
      deste ponto da ordem de trabalhos, sem direito a voto.
1. O sócio que seja admitido compromete-se a comunicar à Direcção qualquer
   alteração nos dados constantes do Formulário de Candidatura.

### Artigo Quarto

(Pena de Exclusão)

1. A exclusão terá de ser fundada em violação grave e culposa dos Estatutos ou
   deste Regulamento Interno.
1. A exclusão terá de ser precedida de processo escrito, do qual constem a
   indicação das infracções, a sua qualificação, a prova produzida, a defesa do
   arguido e a proposta de aplicação da medida de exclusão.
1. O processo previsto no número anterior não se aplica quando a causa de
   exclusão consista na violação do ponto 3, alínea c), do artigo Oitavo dos
   Estatutos, complementado pelo ponto 5, alínea c), do Artigo Segundo deste
   Regulamento Interno.
1. É insuprível a nulidade resultante:
   1. Da falta de audiência do arguido;
   1. Da insuficiente individualização das infracções imputadas ao arguido;
   1. Da falta de referências aos preceitos legais, estatutários ou
      regulamentares, violados;
   1. Da omissão de quaisquer diligências essenciais para a descoberta da
      verdade.
1. A proposta de exclusão a exarar no processo será fundamentada e notificada
   por escrito ao arguido, com a antecedência de, pelo menos, sete dias, em
   relação à data da assembleia geral que sobre ela deliberará.
1. A exclusão deve ser deliberada no prazo máximo de um ano a partir da data em
   que algum membro da direcção tomou conhecimento do facto que a permite.
1. Da deliberação da assembleia geral que decidir a exclusão cabe sempre
   recursos para os tribunais.

### Artigo Quinto

(Pena de Suspensão)

1. Na instauração de qualquer pena de suspensão, aplicam-se, com as devidas
   adaptações, os preceitos dos pontos 1, 2, 4 e 6 do artigo anterior, excepto
   no caso de violação da alínea b) do Artigo Sétimo dos Estatutos,
   complementado pelo pelo ponto 6, alínea b), do Artigo Segundo deste
   Regulamento Interno, em que se prescinde do processo referido no ponto 2 do
   artigo anterior.
1. A decisão de suspensão será fundamentada e notificada por escrito ao sócio,
   contendo obrigatoriamente a indicação da duração da pena de suspensão.
1. Desta decisão pode o sócio suspenso recorrer para a Assembleia Geral.
1. As penas de suspensão aplicadas pela Direcção devem ser imediatamente
   comunicadas ao Conselho Fiscal anexando cópia de todo o processo.

## CAPÍTULO II

(Dos Órgãos Sociais)

### Artigo Sexto

(Reuniões)

1. Em todas as reuniões da Mesa da Assembleia Geral, do Conselho Fiscal e da
   Direcção, o respectivo Presidente terá voto de qualidade.
1. As deliberações dos Órgãos são tomadas por maioria simples com a presença de
   mais de metade dos membros.
1. Será sempre lavrada acta das reuniões de qualquer Órgão, a qual é
   obrigatoriamente assinada por quem exercer funções de presidente e
   disponibilizada no sítio oficial da associação.

### Artigo Sétimo

(Funcionamento da Direcção)

1. A Direcção reúne presencialmente, no mínimo, uma vez por ano, devendo reunir
   via Internet sempre que tal se justificar.
1. Os membros do Conselho Fiscal e da Mesa da Assembleia Geral podem assistir e
   participar nas reuniões da Direcção, sem direito de voto.

### Artigo Oitavo

(Transparência)

1. A associação rege-se pelo princípio da total transparência das fontes e modos de financiamento.
1. Os relatórios e contas e o relatório de actividades da Associação são públicos e devem estar disponíveis no sítio (site) oficial da Associação.

## CAPÍTULO III

(Do Processo Eleitoral)

### Artigo Nono

(Eleições)

1. Os membros da Mesa da Assembleia Geral, os membros da Direcção e os membros
   do Conselho Fiscal, são eleitos bienalmente por escrutínio secreto.
1. Compete à Mesa da Assembleia Geral estabelecer a data das eleições, tendo em
   conta que esta se deverá realizar dois anos após a eleição anterior com uma
   tolerância de 15 dias antes ou depois.
1. A Mesa da Assembleia Geral deverá comunicar a todos os sócios, por correio
   electrónico e colocando um aviso no sítio electrónico oficial da ANSOL, com
   pelo menos 60 dias de antecedência, a data marcada para as eleições.
1. As candidaturas às eleições deverão ser organizadas com base em listas de
   candidatos, apresentadas e aceites nos termos do presente Regulamento.
1. A convocatória da Assembleia Geral eleitoral será expedida com pelo menos 15
   dias de antecedência.

### Artigo Décimo

(Preparação e fiscalização do acto eleitoral)

1. Os actos preparatórios e a orientação, fiscalização e direcção do acto
   eleitoral competem à Mesa da Assembleia Geral, que funcionará como Comissão
   Eleitoral, a que serão agregados os vogais verificadores a que se refere o
   número 2 do artigo Décimo Segundo, cabendo aos secretários a função de
   escrutinadores.
1. Não existindo Mesa de Assembleia Geral, por ter sido destituída ou ter-se
   demitido, os actos preparatórios do acto eleitoral serão dirigidos pelo
   Presidente do Conselho fiscal, ou, na falta deste, pelo Presidente da
   Direcção ou órgão que exerça as funções de gestão da Associação, auxiliado
   por dois membros dos respectivos órgãos, de sua escolha, funcionando como
   Comissão Eleitoral nos termos do número 1 deste artigo, e a Mesa do acto
   eleitoral será constituída por quem a Assembleia Geral eleitoral designar na
   ocasião, mas fazendo sempre parte dela os vogais verificadores, a que se
   refere o número anterior.
1. Na falta de secretários da Mesa, o Presidente da Assembleia Geral escolherá
   de entre os associados, aquele ou aqueles que forem necessários para
   constituir a Comissão Eleitoral.

### Artigo Décimo Primeiro

(Cadernos Eleitorais)

Compete à Comissão Eleitoral eleborar o Caderno Eleitoral, que será composto
por todos os sócios no pleno gozo dos seus direitos.

### Artigo Décimo Segundo

(Apresentação de candidaturas)

1. Na apresentação das candidaturas, os proponentes deverão indicar qual de
   entre eles exercerá as funções de vogal verificador e fará parte da Comissão
   Eleitoral como seu representante, bem como o respectivo suplente.

### Artigo Décimo Terceiro

(Regularidade das candidaturas)

1. A apresentação das candidaturas será feita ao Presidente da Comissão
   Eleitoral em carta, que deverá dar entrada na sede social ou em delegação da
   Associação até trinta dias antes da data para a qual tiver sido convocado o
   acto eleitoral.
1. No dia imediato, deverá a Comissão Eleitoral, reunida com os vogais
   verificadores, comprovar a conformidade das candidaturas com os estatutos e
   o presente regulamento.
1. Se for detectada alguma irregularidade, o vogal verificador representante da
   respectiva candidatura disporá das quarenta e oito horas seguintes para a
   sua correcção, sob pena da mesma não poder ser considerada.
1. Verificando-se irregularidade em qualquer candidatura e não estando presente
   o vogal verificador seu representante, a candidatura será anulada.
1. Não havendo candidaturas válidas para todos ou alguns dos órgãos ou cargos
   elegendos, o Presidente da Comissão Eleitoral notificará a Direcção em
   exercício, que fica obrigada a propôr as candidaturas em falta no prazo de
   quarenta e oito horas.
1. Das decisões da Comissão Eleitoral, que serão tomadas por maioria, cabendo a
   cada membro um voto e ao Presidente voto de qualidade, cabe recurso para a
   Assembleia Geral, que será apreciado como ponto prévio à realização do acto
   eleitoral.

### Artigo Décimo Quarto

(Relação das candidaturas: boletins de voto)

1. Quinze dias antes da data para a qual tiver sido convocado o acto eleitoral,
   o Presidente da Comissão Eleitoral promoverá a afixação na sede social e no
   sítio oficial da Associação, depois de assinada pela Comissão Eleitoral, a
   relação das candidaturas aceites, em conformidade com as quais serão
   elaborados os boletins de voto.
1. As candidaturas serão diferenciadas por letras, correspondendo a ordem
   alfabética à ordem cronológica da respectiva apresentação.
1. A partir das listas definitivas os serviços da Associação providenciarão
   pela elaboração de boletins de voto, que serão postos à sua disposição no
   local em que se realizar o acto eleitoral, e que serão de aspecto
   absolutamente igual para todas as listas.
1. Os processos das candidaturas ficarão arquivados na sede da Associação e
   deles constarão todos os documentos respeitantes a cada candidatura, e entre
   eles as actas das reuniões da Comissão Eleitoral.

### Artigo Décimo Quinto

(Votação)

A votação será por escrutínio secreto e decorrerá no local referido na
convocatória, segundo o horário nela indicado, só podendo votar os sócios
constantes do caderno eleitoral a que se refere o artigo Décimo Primeiro.

### Artigo Décimo Sexto

(Proclamação das listas mais votadas)

1. A proclamação das listas mais votadas no escrutínio será feita logo após o
   apuramento ser comunicado a todos os sócios.
1. Se, para cada órgão social, nenhuma das listas alcançar a maioria absoluta
   de votos expressos, a acto eleitoral será repetido catorze dias mais tarde,
   concorrendo apenas as duas listas mais votadas.
1. Verificando-se a necessidade de repetição do acto eleitoral, este será
   realizado, sempre que possível, no mesmo local e à mesma hora, devendo tal
   ser comunicado verbalmente à Assembleia pelo Presidente da Mesa. Os serviços
   da Associação providenciarão para que tal facto seja comunicado a todos os
   sócios.

### Artigo Décimo Sétimo

(Conclusão dos trabalhos: reclamações)

1. Findos os trabalhos, a Mesa da Assembleia Eleitoral redigirá a respectiva
   acta, que será assinada por todos os seus membros.
1. Quaisquer reclamações sobre o acto eleitoral deverão ser presentes à Mesa da
   Assembleia Eleitoral, nas quarenta e oito horas seguintes, a qual
   funcionando como órgão de fiscalização, decidirá nas vinte e quatro horas
   seguintes, comunicando por escrito a sua decisão aos reclamantes.
1. Da decisão tomada nos termos do número anterior, cabe recurso aos tribunais.
1. Os vogais verificadores, efectivos e suplentes, cessam automaticamente as
   funções com o decurso do prazo para apresentação de reclamações, quando não
   haja, ou após a decisão sobre as que tenham sido apresentadas.

## CAPÍTULO IV

(Dos Grupos de Trabalho)

### Artigo Décimo Oitavo

(Criação)

Para melhor levar a cabo as actividades a que se propõe, pode a Direcção
designar Grupos de Trabalho diferenciados.

### Artigo Décimo Nono

(Fins)

Os Grupos de Trabalho têm por fim a intervenção nas respectivas áreas de
actividade, estabelecidas quando da criação de cada Grupo de Trabalho e
definição do respectivo âmbito.

### Artigo Vigésimo

(Competências)

Compete aos Grupos de Trabalho:

<ol class='alpha'>
<li>Levar a cabo as actividades que se enquadrem no seu âmbito;</li>
<li>Dinamizar a intervenção dos respectivos membros na vida associativa;</li>
<li>Propor à Direcção a tomada de posições internas à Associação ou públicas
   sobre matérias do respectivo âmbito de actividades.</li>
</ol>

### Artigo Vigésimo Primeiro

(Composição)

1. Os Grupos de Trabalho são compostos por todos os associados interessados nas
   respectivas actividades ou que às mesmas queiram dar o seu contributo
   pessoal.
1. Os Grupos de Trabalho podem integrar ainda elementos não-associados, sempre
   que a sua participação se justifique.

### Artigo Vigésimo Segundo

(Coordenação)

1. A Direcção deverá estar representada, por um seu elemento, em cada um dos
   Grupos de Trabalho.
1. Este elemento fará a ponte entre o Grupo de Trabalho e a Direcção,
   mantendo-a informada das actividades do grupo.
