---
categories: []
metadata:
  mapa:
  - mapa_geom: !binary |-
      AQEAAAAAAAAwMpshwKZXUIKJ3kNA
    mapa_geo_type: point
    mapa_lat: !ruby/object:BigDecimal 27:0.39738571442823e2
    mapa_lon: !ruby/object:BigDecimal 27:-0.8803117275238e1
    mapa_left: !ruby/object:BigDecimal 27:-0.8803117275238e1
    mapa_top: !ruby/object:BigDecimal 27:0.39738571442823e2
    mapa_right: !ruby/object:BigDecimal 27:-0.8803117275238e1
    mapa_bottom: !ruby/object:BigDecimal 27:0.39738571442823e2
    mapa_geohash: ez1dpdjvbqxupurc
  slide:
  - slide_value: 1
  node_id: 679
  event:
    location: Casulo Lounge Caffé
    site:
      title: 
      url: 
    date:
      start: 2019-07-13 19:00:00.000000000 +01:00
      finish: 2019-07-13 23:00:00.000000000 +01:00
    map: {}
layout: evento
title: Festa do Software Livre 2019
created: 1562325735
date: 2019-07-05
aliases:
- "/evento/679/"
- "/festadosoftware2019/"
- "/node/679/"
---
<p>Estimados Sócios e Simpatizantes<br><br>Este ano a Festa do Software Livre 2019 vai ter um formato diferente.<br>A nossa aposta para este ano é um convívio social, o local escolhido é<br>Leiria um pouco para fugir ao Porto e Lisboa.<br>Vamos encontramos no Casulo (bar) no dia 13/07/2019 (Sábado) às 19:00 e<br>ficar por lá.<br>No Casulo são servidos snaks, tostas, hamburguês entre outras coisas,<br>mas também é possível uma refeição de prato por 5€ na refeição não estão<br>incluída qualquer tipo de bebida, e necessita de reserva.<br><br><br>Ementa possível:<br>Arroz Pato<br>Bacalhau com Natas<br>Bitoque<br><br>Agradecemos a vossa confirmação até dia 09-07-2019, por email, para <strong>contacto(at)ansol.org</strong> .<br><br><br>Como chegar ao Casulo Lounge Caffé,<br><br>Avenida General Humberto Delgado Nº 215 Marquês de Pombal, Leiria 2410-250, Portugal<br><br>OpenStreetMap<br>https://osm.org/go/b8LKsjyhf?m=<br><br>Geo URI<br>geo:39.73860,-8.80322?z=19<br><br>Maps Open Route Service<br>http://bit.ly/2JbxFoi<br><br></p>
