---
layout: evento
title: Privacy Camp 24
metadata:
  event:
    location: Online / La Tricoterie, Bruxelas, Bélgica
    site:
      url: https://privacycamp.eu/
    date:
      start: 2024-01-24 09:00:00
      finish: 2024-01-24 22:00:00
---

![](cartaz.png)

Privacy Camp is organised by European Digital Rights (EDRi), in collaboration
with its partners the Research Group on Law, Science, Technology & Society
(LSTS) at Vrije Universiteit Brussel (VUB), Privacy Salon vzw, the Institute
for European Studies (IEE) at Université Saint-Louis – Bruxelles, the Institute
of Information Law (IViR) at University of Amsterdam and the Racism and
Technology Center.

The event brings together digital rights advocates, activists as well as
academics and policy-makers from all around Europe and beyond to discuss the
most pressing issues facing human rights online.

Privacy Camp 2024 will take place on Wednesday, 24 January 2024 in a hybrid
format (in Brussels, with online broadcast). Participation is free and
registrations will open in December 2023. The venue in which the conference
will take place is La Tricoterie, rue Théodore Verhaegen 158.
