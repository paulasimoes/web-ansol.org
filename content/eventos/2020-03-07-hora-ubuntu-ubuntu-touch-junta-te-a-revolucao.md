---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 737
  event:
    location: Sintra
    site:
      title: ''
      url: https://www.meetup.com/ubuntupt/events/268756086/
    date:
      start: 2020-03-12 18:30:00.000000000 +00:00
      finish: 2020-03-12 18:30:00.000000000 +00:00
    map: {}
layout: evento
title: Hora Ubuntu - Ubuntu Touch, junta-te à revolução
created: 1583611577
date: 2020-03-07
aliases:
- "/evento/737/"
- "/node/737/"
---

