---
layout: evento
title: Dia do Domínio Público 2024
showcover: false
metadata:
  event:
    location: Biblioteca Nacional de Portugal, Campo Grande, 83, Lisboa
    site:
      url: https://wikimedia.pt/eventos/dia-dominio-publico-2024/
    date:
      start: 2024-01-05 10:00:00
      finish: 2024-01-05 17:00:00
---

![](cartaz.jpg)

A Wikimedia Portugal e a Biblioteca Nacional de Portugal voltam a unir esforços
para a organização do Dia do Domínio Público, um evento anual de comemoração
pela entrada de novos trabalhos no domínio público. O evento decorrerá na BNP,
dia 5 de janeiro, e contará com a presença de Teresa Nobre, da COMMUNIA, da
parte da manhã. Como atividade prática, durante a tarde, haverá uma oficina
focada em Wikisource, o acervo digital de livros e textos-fonte da
infraestrutura Wikimedia, que reúne obras e documentos no domínio público ou
disponibilizados sob licenças livres.

O Dia do Domínio Público celebra-se anualmente no dia 1 de janeiro. Decorridos
todos os prazos de proteção de direito de autor estabelecidos na lei – em regra
70 anos após a morte do seu criador intelectual, ou 70 anos após a data de
criação cujo autor seja desconhecido – diz-se que uma obra cai em domínio
público. Significa que os direitos patrimoniais da obra cessam, deixando de ser
necessário autorização para partilhar e reutilizar esses conteúdos, que passam
a fazer parte do património cultural da sociedade.

Em Portugal, a data tem sido assinalada pela ANSOL – Associação Nacional para o
Software Livre que, desde 2022, desenvolveu com a Wikimedia Portugal uma
iniciativa para melhorar a qualidade da informação da Wikipédia relativa aos
autores portugueses que entram em domínio público a cada ano. A Biblioteca
Nacional de Portugal associa-se a esta iniciativa e, pelo segundo ano
consecutivo, disponibiliza uma lista de autores portugueses que morreram em
1953, cujas obras passam, assim, a estar em domínio público, a partir de
janeiro de 2024.

A celebração do domínio público continuará ao longo de todo o mês de janeiro
com uma maratona de edição Wikipédia assíncrona, organizada em cooperação com a
Wikimedia España, dedicada a pessoas portuguesas e espanholas cujas obras
entram no domínio público em 2024.
