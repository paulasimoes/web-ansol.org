---
categories: []
metadata:
  node_id: 61
  event:
    location: 
    site:
      title: ''
      url: http://www.faunalia.pt/cursowebmapping
    date:
      start: 2012-05-28 00:00:00.000000000 +01:00
      finish: 2012-05-30 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: 'Web Mapping (WebSIG) e serviços standard OGC: WMS, WFS e WFS-T'
created: 1335974647
date: 2012-05-02
aliases:
- "/evento/61/"
- "/node/61/"
---
<p>Web Mapping (WebSIG) e servi&ccedil;os standard OGC: WMS, WFS e WFS-T: 24<br />
	Horas/3 Dias - 28 a 30 Maio - &Eacute;vora<br />
	<br />
	Este curso permite aprender a usar um servidor de mapas, para poder<br />
	publicar os pr&oacute;prios dados geogr&aacute;ficos na Web. O curso tem a finalidade<br />
	de fornecer os conceitos fundamentais e a experi&ecirc;ncia pr&aacute;tica para<br />
	incorporar cartografia interactiva num s&iacute;tio Web.</p>
