---
categories:
- hacker meeting
- lisboa
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 285
  - tags_tid: 304
  node_id: 615
  event:
    location: FBAUL - Faculdade das Belas Artes da Universidade de Lisboa, Lisboa,
      Portugal
    site:
      title: CCC - CREATIVE CODING CIRCLE - CÍRCULO DE CÓDIGO CRIATIVO
      url: https://creativecodingcircle.wordpress.com/
    date:
      start: 2018-06-04 20:30:00.000000000 +01:00
      finish: 2018-06-04 22:30:00.000000000 +01:00
    map: {}
layout: evento
title: 0x05 - CCC - CREATIVE CODING CIRCLE - CÍRCULO DE CÓDIGO CRIATIVO
created: 1524177946
date: 2018-04-19
aliases:
- "/evento/615/"
- "/node/615/"
---
<h2 class="widget-title">O quê, como e quando?</h2><div class="textwidget"><p><strong>O CCC é um encontro informal e regular de praticantes e interessados na área da Programação Criativa.<br> </strong>Uma vez por mês, das 20:30 às 22:30, na sala 3.07 da FBAUL. Próximas datas: 26 Fevereiro • 12 Março • 16 Abril • 21 Maio • 04 Junho</p></div><h2 class="widget-title">O CCC é organizado por</h2><ul><li><img src="https://0.gravatar.com/avatar/c83a106b3395beaffc24d4da9f4d6e31?s=16&amp;d=identicon&amp;r=G" alt="" class="avatar avatar-16 grav-hashed grav-hijack" id="grav-c83a106b3395beaffc24d4da9f4d6e31-0" width="16" height="16"> <strong>António de Sousa Dias</strong></li><li><img src="https://1.gravatar.com/avatar/1446b7cffc63f9c3b54ab17d934cb384?s=16&amp;d=identicon&amp;r=G" alt="" class="avatar avatar-16 grav-hashed grav-hijack" id="grav-1446b7cffc63f9c3b54ab17d934cb384-0" width="16" height="16"> <strong>bzzerra</strong></li><li><a href="https://creativecodingcircle.wordpress.com/author/monicamendes/"> <img src="https://1.gravatar.com/avatar/768d0f0e73c982aae87a3c1ffd0305bd?s=16&amp;d=identicon&amp;r=G" alt="" class="avatar avatar-16 grav-hashed" id="grav-768d0f0e73c982aae87a3c1ffd0305bd-4" width="16" height="16"> <strong>monicamendes</strong></a></li><li><img src="https://2.gravatar.com/avatar/5d7a773192455cf7b7334bac73b1cbe0?s=16&amp;d=identicon&amp;r=G" alt="" class="avatar avatar-16 grav-hashed grav-hijack" id="grav-5d7a773192455cf7b7334bac73b1cbe0-0" width="16" height="16"> <strong>Pedro Ângelo</strong></li><li><img src="https://0.gravatar.com/avatar/3efc6c35bf319cd75120fda23f484e1c?s=16&amp;d=identicon&amp;r=G" alt="" class="avatar avatar-16 grav-hashed grav-hijack" id="grav-3efc6c35bf319cd75120fda23f484e1c-0" width="16" height="16"> <strong>srafael1976</strong></li><li><img src="https://2.gravatar.com/avatar/84f410b843a7107d1618a5f245d0ba9b?s=16&amp;d=identicon&amp;r=G" alt="" class="avatar avatar-16 grav-hashed grav-hijack" id="grav-84f410b843a7107d1618a5f245d0ba9b-0" width="16" height="16"> <strong>Tiago Rorke</strong></li></ul><h2 class="widget-title">creativecodingcircle@gmail.com</h2>
