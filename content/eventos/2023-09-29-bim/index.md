---
layout: evento
title: "Building Information Modelling (BIM)"
metadata:
  event:
    date:
      start: 2023-09-29
      finish: 2023-09-29
    location: Centro Cultural de Vila Flor, Guimarães
    site:
      url: https://www.ccdr-n.pt/noticia/outros-destaques/BIM
---

**Rumo à transformação digital dos processos de licenciamento de obras públicas e particulares**

A CCDR-NORTE, I.P. promove, em conjunto com a buildingSMART Portugal, a sessão
“Building Information Modelling (BIM): Rumo à transformação digital dos
processos de licenciamento de obras públicas e particulares”. O evento, que tem
lugar a 29 de setembro, no Centro Cultural de Vila Flor, em Guimarães, tem como
objetivo o esclarecimento e a apresentação de estudos de caso de adoção, em
Portugal, do conceito “Building Information Modelling” (BIM).

A sessão destina-se às entidades da Administração Local e Regional e a todos os
restantes agentes da Indústria da Construção com a finalidade de refletir sobre
o papel central do BIM na transformação digital neste setor, nomeadamente face
às últimas propostas legislativas de obrigatoriedade do licenciamento em BIM
até 2027.
