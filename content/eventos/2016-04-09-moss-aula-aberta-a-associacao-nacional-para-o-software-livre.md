---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 412
  event:
    location: ISCTE, Lisboa
    site:
      title: ''
      url: http://moss.dcti.iscte.pt/?p=280
    date:
      start: 2016-04-16 11:00:00.000000000 +01:00
      finish: 2016-04-16 13:00:00.000000000 +01:00
    map: {}
layout: evento
title: 'MOSS - Aula Aberta: A Associação Nacional para o Software Livre'
created: 1460217412
date: 2016-04-09
aliases:
- "/MOSS-ANSOL/"
- "/evento/412/"
- "/node/412/"
---
<p>Marcos Marado, actual Presidente da Direcção da ANSOL, irá lecionar no próximo dia 16 de abril uma "Aula Aberta", dirigida aos alunos do Mestrado em Software de Código Aberto, mas aberta a todos os interessados, sobre a ANSOL e o Software Livre em Portugal.</p><p>A entrada é livre, no entanto agradecemos o <a href="http://moss.dcti.iscte.pt/?p=280">registo</a> por uma questão de logistica. Obrigado.</p>
