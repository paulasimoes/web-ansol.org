---
layout: evento
title: Encontro Ubuntu-pt Julho @ Sintra
metadata:
  event:
    location: Avenida Movimento das Forças Armadas 5, 2710-436 Sintra, Portugal
    site:
      url: https://loco.ubuntu.com/events/ubuntu-pt/4369-encontro-ubuntu-pt-sintra/
    date:
      start: 2023-07-13 20:00:00.000000000 +01:00
      finish: 2023-07-13 23:00:00.000000000 +01:00
---

Vem conhecer a comunidade, conversar sobre Ubuntu, Software Livre e tecnologia em ambiente de convívio descontraído, e traz um amigo também.

Dia 13 de Julho a partir das 20h no Bar Saloon, em Sintra.
Detalhes e inscrição (não obrigatória) na [página do evento](https://loco.ubuntu.com/events/ubuntu-pt/4369-encontro-ubuntu-pt-sintra/).
