---
layout: evento
title: Festa do Software Livre 2023
metadata:
  event:
    date:
      start: 2023-09-15
      finish: 2023-09-17
    location: Universidade de Aveiro
    site:
      url: https://festa2023.softwarelivre.eu
---

A Festa do Software Livre é um encontro nacional de várias comunidades interessadas em Software Livre. Ao longo dos três dias vamos contar com várias apresentações, painéis e workshops.

A primeira Festa do Software Livre da Moita decorreu nas instalações da extinta Universidade Independente, no Palheirão, em 2004, co-organizada pela Humaneasy Consulting e pela ANSOL – Associação Nacional para o Software Livre, e contou com o apoio do Município da Moita, da Universidade Independente e de várias entidades e comunidades ligadas ao Software Livre.

A segunda edição da Festa do Software Livre, organizada em 2018 também pela Humaneasy Consulting e pela ANSOL com o apoio do Munícipio da Moita, incluiu um programa de palestras, demonstrações, workshops, informação, hackathons, zona para instalação de software livre, animação com música ao vivo e outras atracções. Durante o evento contámos com a presença e colaboração da Comunidade Ubuntu Portugal, Wikimedia Portugal, OSGeo Portugal, Drupal Portugal, D3, RepRap, Linux Tech, entre outros.

Em 2023 a festa regressa para a sua terceira edição, englobando o Dia do Software Livre, e juntando diversas comunidades no campus Universitário de Aveiro para três dias de apreentações e partilha.

A ENTRADA É LIVRE E GRATUITA.
