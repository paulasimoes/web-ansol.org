---
layout: evento
title: Wikicon Portugal 2024
metadata:
  event:
    location: Évora
    site:
      url: https://wikimedia.pt/eventos/wikicon-portugal-2024/
    date:
      start: 2024-04-05
      finish: 2024-04-07
---

A Wikicon Portugal é o encontro do movimento wikimedista no país. Em 2024,
decorrerá de 5 a 7 de abril, em Évora.


Chamada para propostas de programação para a Wikicon Portugal 2024:
https://wikimedia.pt/2023/11/23/abriu-a-chamada-para-propostas-de-programacao-para-a-wikicon-portugal-2024/

Mais informações em breve.
