---
layout: evento
title: Introdução ao Freedcad para impressão 3D - Primeiros Passos
metadata:
  event:
    date:
      start: 2023-06-25 15:30:00.000000000 +01:00
      finish: 2023-06-25 20:00:00.000000000 +01:00
    location: LARGO Residências, Lisboa
    site:
      url: https://www.facebook.com/events/163377873234928/
---

# Introdução ao Freedcad para impressão 3D - Primeiros Passos

Desenhar peças em 3d pode ser difícil, mas se o fizer de uma forma simples e guiada removeremos os primeiros bloqueios libertando-se para um processo de auto-aprendizagem.

Será um workshop para adultos, orientado por Luis Dinis, dia 25 Junho às 15h, no Espaço Coletivo Maker, do Largo Residências - Quartel do Largo do Cabeço de Bola

## Inscrições:
Por email para: lmad.x0 [at] gmail [ponto] com

## Entidade programadora:
B.Lab
