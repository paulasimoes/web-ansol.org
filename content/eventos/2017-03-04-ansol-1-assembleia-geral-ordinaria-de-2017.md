---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 490
  event:
    location: Casa das Associações, Porto
    site:
      title: 
      url: 
    date:
      start: 2017-03-12 14:00:00.000000000 +00:00
      finish: 2017-03-12 14:00:00.000000000 +00:00
    map: {}
layout: evento
title: ANSOL - 1ª Assembleia Geral Ordinária de 2017
created: 1488664503
date: 2017-03-04
aliases:
- "/AG2017/"
- "/evento/490/"
- "/node/490/"
---
<p>A Mesa da Assembleia Geral convoca todos os sócios da ANSOL - Associação Nacional para o Sofware Livre a comparecer à 1ª Assembleia Geral Ordinária de 2017, que terá lugar na sede da associação.</p><!-- break --><p>Tendo lugar na Casa das Associações no Porto, onde a ANSOL se encontra sediada, no dia 12 de Março, com início às 14h00 da tarde, a Assembleia Geral terá a seguinte ordem de trabalhos:</p><ul><li>Apresentação e aprovação do Relatório de actividades e Contas de 2016</li><li>Apresentação e aprovação do plano de actividades de 2017</li><li>Outros Assuntos</li></ul><p>Se à hora marcada não estiverem presentes, pelo menos, metade dos associados a Assembleia Geral reunirá, em 2ª convocatória, no mesmo local e passados 30 minutos, com qualquer número de presenças.</p><p><strong>Morada:</strong></p><p style="margin-left: 30px;">Casa das Associações<br class="gmail_msg"> Federação das Associações Juvenis do Distrito do Porto<br class="gmail_msg"> Rua Mouzinho da Silveira, 234/6/8<br class="gmail_msg"> 4050-017 Porto</p><p>(Na baixa do Porto, descendo da estação de S.Bento para a Ribeira)</p>
