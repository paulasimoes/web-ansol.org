---
categories: []
metadata:
  node_id: 16
  event:
    location: Lisboa
    site:
      title: Microestruturas - Culture Freedom Day
      url: http://www.flausina.com/sonicarea.html#microestruturas
    date:
      start: 2012-05-11 00:00:00.000000000 +01:00
      finish: 2012-05-12 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Microestruturas - Culture Freedom Day
created: 1333182193
date: 2012-03-31
aliases:
- "/evento/16/"
- "/node/16/"
---
O Culture Freedom Day é uma iniciativa para que se celebre anualmente a Liberdade na Cultura. Celebrado pela primeira vez em 2012, as celebrações em Lisboa serão antecipadas, ocorrendo nos dias 11 e 12 de Maio, inseridas no programa "Microstructures", na Flausina.
