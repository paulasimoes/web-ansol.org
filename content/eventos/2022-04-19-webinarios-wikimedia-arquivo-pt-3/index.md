---
layout: evento
title: "Arquivar a Web: faça-você-mesmo! (3º Webinar)"
metadata:
  event:
    date:
      start: 2022-04-19 15:00:00
      finish: 2022-04-19 16:30:00
    location: Online
    site:
      url: https://forms.gle/Q2sSKS2thWuS7AiC6
---

Mais informação na [página do programa](https://blog.wikimedia.pt/2022/02/08/ciclo-de-webinarios-em-colaboracao-com-o-arquivo-pt/).

## Património cultural na Web: como preservar as referências na Wikipédia?

> O objetivo deste ciclo de Webinars é apresentar os serviços do Arquivo.pt e
> disseminar a sua utilização para que o património histórico publicado na web
> possa ser preservado e explorado por qualquer cidadão.
>
> Este ciclo de Webinars, dedicado à preservação da memória cultural publicada na
> Web, é uma colaboração entre a Wikimedia Portugal e o Arquivo.pt (Fundação para
> a Ciência e a Tecnologia I.P.).


## Arquivar a Web: faça-você-mesmo! (3º Webinar)

> Resumo: Webinar que apresenta como é preservada a informação cultural de índole
> municipal e nacional publicada na Web. Demonstra através de casos práticos como
> qualquer pessoa pode arquivar informação publicada na web num formato adequado
> que permitirá a sua preservação para o futuro utilizando ferramentas gratuitas.
> Este Webinar destina-se a qualquer utilizador da Internet mas é particularmente
> útil para responsáveis pela comunicação e gestão de informação em organizações.
> [Saber mais](https://sobre.arquivo.pt/pt/ajuda/formacao/modulo-d/)
