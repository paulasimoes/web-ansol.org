---
layout: evento
title: Wikidata Days 2024
showcover: false
metadata:
  event:
    date:
      start: 2024-11-14
      finish: 2024-11-16
    location: Biblioteca Nacional de Portugal
    site:
      url: https://wikidatadays.wikimedia.pt
---

![Logotipo do Wikidata Days 2024](cover.png)

O Wikidata Days tem a sua génese nos GLAM Days, realizados na Biblioteca Nacional de Portugal em 2018. Desde então, anualmente têm vindo a realizar-se vários eventos em torno do Wikidata e das parceiras entre os GLAM (Galleries, Libraries, Archives and Museums) e a comunidade científica que utilizam o Wikidata como repositório de dados estruturados abertos.

O Wikidata é um projeto colaborativo da Wikimedia Foundation, a mesma organização responsável pela Wikipédia. Tem como objetivo criar uma base de conhecimento estruturada e aberta, que possa ser utilizada tanto por humanos como por máquinas, permitindo a partilha de dados de forma ligada, fazendo parte da web semântica.

Ao longo de três dias, serão apresentadas comunicações, workshops de edição e aplicações práticas para organização de dados. Este evento, que se pretende que mantenha uma periodicidade anual, procura reunir especialistas nestas temáticas, abordando melhores práticas na utilização do Wikidata em projetos GLAM e de investigação e refletindo sobre o seu impacto na disseminação global do conhecimento.
