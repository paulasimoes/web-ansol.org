---
layout: evento
title: Pycon Portugal 2023
metadata:
  event:
    date:
      start: 2023-09-07
      finish: 2023-09-09
    location: Coimbra Business School - ISCAC
    site:
      url: https://2023.pycon.pt/
---

Temos o prazer de anunciar a próxima edição da PyCon Portugal!

Marquem na agenda: 7, 8 e 9 de Setembro, na Coimbra Business School.

O evento deste ano promete ser maior e melhor do que nunca! [PyconPT](https://2023.pycon.pt)

![We're on! Save the Date: PyCon Portugal 2023 7-9 September, Coimbra Business School](cartaz.png)
