---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 391
  event:
    location: Rua dos Mercadores, 3 - Sotão da Associação Agor@, Aveiro, Portugal
    site:
      title: 'Hack''Aveiro #68 - ♬!♬! Solder on, brave maker!!! ♬!! ♬!'
      url: https://plus.google.com/events/cvebah9h7dfvn8toedjasan6hgs
    date:
      start: 2016-01-20 21:00:00.000000000 +00:00
      finish: 2016-01-20 23:45:00.000000000 +00:00
    map: {}
layout: evento
title: 'Hack''Aveiro #68 - ♬!♬! Solder on, brave maker!!! ♬!! ♬!'
created: 1453074814
date: 2016-01-17
aliases:
- "/evento/391/"
- "/node/391/"
---
<p>Encontro semanal de Makers, Hackers e Artistas da Associação HackAveiro.&nbsp;Encontro aberto a todos.</p><p>&nbsp;</p><p><strong>Aonde?</strong></p><p>Rua dos Mercadores, 3 - Sotão da Associação Agor@</p><p><strong>Quando?</strong></p><p>20 de Janeiro, 21:00</p><p>&nbsp;</p><p>Traz as tuas ideias, invenções e novidades!!</p>
