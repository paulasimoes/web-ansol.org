---
layout: evento
title: Workshop de Introdução ao Scratch @ Centro Linux
metadata:
  event:
    location: MILL – Makers In Little Lisbon Calçada do Moinho de Vento, 14B, 1150-236 Lisboa
    site:
      url: https://scratch.centrolinux.pt
    date:
      start: 2023-01-28
      finish: 2023-01-28
---

[![Cartaz](cartaz.svg)](https://osm.org/go/b5crq_xVM?layers=N)

# Workshop de Introdução ao Scratch

## Vamos aprender a criar um jogo?

Utilizando o ambiente de programação Scratch, podemos aprender a programar e criar os nossos próprios projetos. Aqui, criar um jogo é muito divertido!

## Inscrições e detalhes

As inscrições e todos os detalhes no site do [evento](https://scratch.centrolinux.pt).
