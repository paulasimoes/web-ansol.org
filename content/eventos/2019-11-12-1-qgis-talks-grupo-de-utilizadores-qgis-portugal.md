---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 710
  event:
    location: Coimbra
    site:
      title: ''
      url: http://qgis.pt/eventos_qgistalk.html
    date:
      start: 2019-11-16 00:00:00.000000000 +00:00
      finish: 2019-11-16 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: 1º QGIS Talks Grupo de Utilizadores QGIS Portugal
created: 1573558251
date: 2019-11-12
aliases:
- "/evento/710/"
- "/node/710/"
---

