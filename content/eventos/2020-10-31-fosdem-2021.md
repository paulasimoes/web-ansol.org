---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 761
  event:
    location: Online
    site:
      title: ''
      url: https://fosdem.org/2021/
    date:
      start: 2021-02-06 00:00:00.000000000 +00:00
      finish: 2021-02-07 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: FOSDEM 2021
created: 1604167228
date: 2020-10-31
aliases:
- "/evento/761/"
- "/node/761/"
---

