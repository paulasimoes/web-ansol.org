---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 308
  event:
    location: Parlamento Europeu
    site:
      title: ''
      url: http://www.europarl.europa.eu/committees/en/inta/calendar.html
    date:
      start: 2015-05-28 00:00:00.000000000 +01:00
      finish: 2015-05-28 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Voto no INTA sobre o TTIP
created: 1429126985
date: 2015-04-15
aliases:
- "/evento/308/"
- "/node/308/"
---

