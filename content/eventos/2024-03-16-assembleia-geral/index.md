---
layout: evento
title: Assembleia Geral Eleitoral 2024
showcover: false
metadata:
  event:
    location: MILL - Makers In Little Lisbon, Lisboa
    site:
      url: https://ansol.org/noticias/2024-01-16-assembleia-geral-eleitoral/
    date:
      start: 2024-03-16 15:00:00
      finish: 2024-03-16 19:00:00
---

A Assembleia Geral Eleitoral 2024 da ANSOL terá lugar no “MILL - Makers In
Little Lisbon”, no dia 16 de Março de 2024, com início às 15h00 da tarde.

Os detalhes da convocatória encontram-se afixados na página anexada.
