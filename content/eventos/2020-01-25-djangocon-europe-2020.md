---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 729
  event:
    location: Porto
    site:
      title: ''
      url: https://2020.djangocon.eu/
    date:
      start: 2020-09-16 00:00:00.000000000 +01:00
      finish: 2020-09-20 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: DjangoCon Europe 2020
created: 1579915883
date: 2020-01-25
aliases:
- "/evento/729/"
- "/node/729/"
---

