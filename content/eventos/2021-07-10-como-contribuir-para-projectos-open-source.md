---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 806
  event:
    location: Online
    site:
      title: ''
      url: https://meet.jit.si/Palestra_ANSOL
    date:
      start: 2021-07-14 18:30:00.000000000 +01:00
      finish: 2021-07-14 18:30:00.000000000 +01:00
    map: {}
layout: evento
title: Como contribuir para projectos Open Source
created: 1625930796
date: 2021-07-10
aliases:
- "/evento/806/"
- "/node/806/"
---
A convite do <a href="https://glua.ua.pt/">GLUA - Grupo de Utilizadores de
Linux da Universidade de Aveiro</a>, a ANSOL vai palestrar aos interessados -
online - sobre "Como contribuir para projectos Open Source".

<img src="https://ansol.org/attachments/ansol.png" alt="poster do evento">
