---
categories:
- ttip
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 54
  node_id: 321
  event:
    location: 
    site:
      title: ''
      url: https://www.nao-ao-ttip.pt/debate-dia-14-de-maio-fcsh-lisboa/
    date:
      start: 2015-05-14 16:00:00.000000000 +01:00
      finish: 2015-05-14 16:00:00.000000000 +01:00
    map: {}
layout: evento
title: Impacto do TTIP em Portugal e na Europa
created: 1431513918
date: 2015-05-13
aliases:
- "/evento/321/"
- "/node/321/"
---

