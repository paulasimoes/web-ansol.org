---
categories: []
metadata:
  node_id: 129
  event:
    location: Em todo o lado
    site:
      title: ''
      url: http://fsfe.org/campaigns/ilovefs/ilovefs.pt.html
    date:
      start: 2013-02-14 00:00:00.000000000 +00:00
      finish: 2013-02-14 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Dia do Amor ao Software Livre
created: 1360197747
date: 2013-02-07
aliases:
- "/evento/129/"
- "/node/129/"
---
<p>O Dia de S&atilde;o Valentim &eacute; tradicionalmente um dia para mostrar e celebrar o amor. Porque n&atilde;o aproveitar esta oportunidade para mostrar o seu amor pelo Software Livre este ano?</p>
