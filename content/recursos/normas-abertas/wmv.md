---
categories:
- normas abertas
layout: article
title: É o WMV uma norma aberta?
date: 2015-03-17
aliases:
- "/article/295/"
- "/node/295/"
---

De acordo com a Lei 36/2011, uma norma é aberta se cumprir cumulativamente os
seguintes requisitos:

<style type="text/css">
@counter-style alinea {
  system: cyclic;
  symbols: a b c d e f g h i j k l m n o p q r s t u v w x y z;
  suffix: ") ";
}
ol { list-style-type: alinea; }
</style>

1. A sua adopção decorra de um processo de decisão aberto e disponível à
   participação de todas as partes interessadas;
1. O respectivo documento de especificações tenha sido publicado e livremente
   disponibilizado, sendo permitida a sua cópia, distribuição e utilização, sem
   restrições;
1. O respectivo documento de especificações não incida sobre acções ou
   processos não documentados;
1. Os direitos de propriedade intelectual que lhe sejam aplicáveis, incluindo
   patentes, tenham sido disponibilizados de forma integral, irrevogável e
   irreversível ao Estado Português;
1. Não existam restrições à sua implementação.


Tendo os dois primeiros pontos em consideração, observa-se que:

* O WMV foi aprovado como norma pela SMTPE, pelo que à partida, o ponto (a)
  estará cumprido;
* O documento de especificações não é livremente disponibilizado. Segundo o <a
  href="https://www.smpte.org/news-events/news-releases/smpte-releases-vc-1-standard">sítio
  web da SMTPE</a>:
  > The VC-1 documents are SMPTE 421M-2006, "VC-1 Compressed Video Bitstream Format and Decoding Process" - the Standard itself, as well as two supporting Recommended Practices, SMPTE RP227-2006 "VC-1 Bitstream Transport Encodings" and SMPTE RP228-2006 "VC-1 Decoder and Bitstream Conformance". All three documents can be purchased on the SMPTE website at www.smpte.org.".

Assim, o ponto b) não é cumprido, pelo que **a norma WMV não é considerável aberta**, segundo a Legislação Nacional.
