---
categories:
- newsletter
- boletim
layout: article
title: Boletim 2023 - 3º trimestre
date: 2023-11-13
---

Neste trimestre, o nosso esforço foi dirigido para a organização da Festa do
Software Livre, um evento nacional de três dias que juntou várias comunidades
do Software Livre em Aveiro.

Além disso, organizámos um jantar de celebração do 30º aniversário do Debian,
estivemos presentes com uma banca no ENEI 2023, e participámos nas campanhas do
Chat Control.

<!--more-->

## Falecimento de André Esteves

A ANSOL e a comunidade do Software Livre perderam um membro da sua comunidade.

No dia 13 de Setembro, após uma longa batalha contra o cancro, o nosso
associado e vogal da direcção André Esteves faleceu.

Decidimos dedicar a Festa do Software Livre 2023 à sua memória, relembrando e
agradecendo todo o trabalho que desenvolveu nos vários cargos que desempenhou
em órgãos sociais da ANSOL, desde 2001.

## Mudança de Vogal da Direcção

Dia 4 de outubro a direcção reuniu e deliberou, de acordo com o ponto 4 do
artigo 9.º dos Estatutos, nomear o sócio Pedro Gaspar como Vogal, com efeitos
imediatos.

Queremos agradecer ao Pedro Gaspar e a todos os restantes sócios que se
disponibilizaram para preencher a vaga.

## Festa do Software Livre 2023

![Colagem de fotografias da Festa do Software Livre: ](fsl.png)

A Festa do Software Livre 2023 foi um encontro nacional de várias comunidades
interessadas em Software Livre. Ao longo dos três dias contámos com várias
apresentações, painéis e workshops.

A edição de 2023 contou com **17 tracks** paralelas ao longo de **três dias**,
num total de **mais de 100 apresentações e workshops**. Ao longo dos três dias,
registámos **mais de 300 participantes no evento.**


## Encontro Nacional de Estudantes de Informática 2023

![Foto de 14 estudantes a pedir informações na banca da ANSOL durante o ENEI](enei.jpg)

Durante o ENEI 2023, tivemos a oportunidade de ter uma banca na mostra de
empresas do evento. Aproveitámos esta oportunidade para divulgar entre
estudantes o nome da ANSOL e explicar o que é e porque é importante Software
Livre. Também falámos sobre DRM, ChatControl, e Public Money Public Code.


## Chat Control

Numa iniciativa liderada pela [D3](https://direitosdigitais.pt/), lançámos um
campanha contra uma proposta europeia que compromete encriptação das
comunicações.

Podem saber mais sobre esta campanha no website <https://ChatControl.pt>.


## Jantar do 30º Aniversário do Debian

Para celebrar o trigésimo aniversário do projecto Debian, organizámos um jantar
social em Lamego.


## Segue as actividades da ANSOL

Podem seguir as actividades através da nossa presença em várias redes:

- Segue-nos no Mastodon: <https://floss.social/@ansol>
- Segue-nos no Peertube: <https://viste.pt/c/ansol/>
- Segue-nos no Twitter: <https://twitter.com/ANSOL>
- Junta-te à nossa sala via Matrix: <https://matrix.to/#/#geral:ansol.org>
- Vê os nossos repositórios de Software Livre: <https://git.ansol.org/ansol>

Se consideras importante o tema de Software Livre na sociedade, considera
[juntar-te à ANSOL](https://ansol.org/inscricao/). A ANSOL depende
exclusivamente da contribuição dos seus membros para suportar as suas
actividades, e gostaríamos de contar com a tua participação.
