---
layout: article
title: "Software Livre nas Eleições Europeias 2024"
date: 2024-06-04
image:
  caption: |
    Imagem adaptada de ["Europe Day: visit Parliament during our open days", por Parlamento Europeu](https://flickr.com/photos/european_parliament/53695830888/), sob a licença CC BY 2.0.
---
A ANSOL enviou um conjunto de questões aos partidos candidatos às Eleições
Europeias 2024 sobre os temas mais prementes em discussão na União Europeia,
que incluiam a Regulação de Patentes Essenciais a Normas, a Criptografia de
Ponta-a-Ponta (E2EE), Dados Abertos, Acesso Aberto e Recursos Educacionais
Abertos, Soberania Digital, e Ecologia e Sustentabilidade.

Publicamos abaixo as respostas completas, por ordem de recepção.

## Regulação de Patentes Essenciais a Normas (SEP)

**Um dos dossiers que se encontra à espera da nova composição parlamentar é a
Regulação de Patentes Essenciais a Normas (Standard Essential Patents
regulation). Há uma posição negocial aprovada pelo Parlamento, mas a proposta
em cima da mesa tem merecido as críticas e preocupações da comunidade de
Software Livre. Qual é a posição da vossa candidatura em relação ao texto que
irá agora ser negociado?**

![](partido-nc.png)
**Nós, Cidadãos!**:
Numa primeira opinião, o meu impulso analítico, leva-me à necessidade da
matéria carecer de ser analizada sobre diversos pontos de análise,
nomeadamente, no âmbito da Cibersegurança, no âmbito do Serviço Público e no
âmbito do uso Profissional lucrativo, sem nunca perder de vista o investimento
inicial do autor da ideia com valor económico -  o qual deve ser adequadamente
remunerado ou protegido por Patente, no mínimo até ao Break Even, acrescido de
um prémio monetário relativo à inovação e utilidade para a maioria das pessoas,
e, acima de tudo, que sejam Softwares Livres de violar a liberdade individual
dos cidadãos utilizadores.


![](partido-cdu.png)
**CDU - Coligação Democrática Unitária:**
O PCP defende a livre partilha do conhecimento. E entende que, quando é
essencial para a melhoria do bem estar dos cidadãos, este deve manter-se na
esfera pública. Assim, não pode estar de acordo com qualquer iniciativa que
vise promover o patenteamento ou facilitar a utilização de elementos
patenteados em normas. Na realidade, se o objectivo é garantir a
interoperabilidade entre sistemas, as normas devem ser o mais abertas possível,
evitando qualquer tipo de constrangimento à sua adopção. Como noutro casos, o
patenteamento só serve para garantir receitas e controlo tecnológico nas mãos
dos grandes monopólios.


![](partido-be.png)
**Bloco de Esquerda:**
Preocupa-nos a falta de transparência tanto na elaboração do texto, como nas
formas propostas de licenciamento de patentes. Somos críticos deste sistema de
maximalização da propriedade intelectual e de todo o aparato jurídico e
repressivo que é construído para o manter. As ambiguidades à volta do que
corresponde ou não à definição de termos FRAND são problemáticas. A própria
inovação é posta em causa com a complexidade do sistema de patentes, sobretudo
nos casos em que os proponentes não tenham margem para iniciar processos
judiciais, privilegiando injustamente empresas e promovendo uma assimetria
prejudicial para um ecossistema económico e científico saudável.

![](partido-vp.png)
**Volt Portugal**:
Em relação à Regulação de Patentes Essenciais a Normas (Standard Essential
Patents regulation), o Volt pretende promover a transparência, a inovação e a
acessibilidade no setor de software, defendendo uma abordagem que equilibre os
interesses dos detentores de patentes essenciais, assim como a comunidade de
Software Livre. O Volt apoiaria medidas que garantam a justa remuneração dos
detentores de patentes, ao mesmo tempo que promovem a interoperabilidade, a
concorrência leal e o acesso aberto ao conhecimento e à tecnologia para
benefício da sociedade em geral. Assim, o Volt estaria disposto a contribuir
para e aprovar uma norma que seguisse estes princípios.

![](partido-ad.png)
**AD - Aliança Democrática**:
O documento "EU Proposal for a Regulation on Standard Essential Patents"
publicado pelo Serviço de Estudos do Parlamento Europeu em novembro de 2023
analisa a proposta da Comissão Europeia para um novo regulamento sobre patentes
essenciais padrão (SEPs). As SEPs são patentes que cobrem tecnologias
necessárias para a implementação de normas de tecnologia. A Comissão Europeia
propôs um novo regulamento sobre SEPs em 2022 com o objetivo de abordar as
preocupações relacionadas ao uso de SEPs por detentores de direitos de
propriedade intelectual (DPI). As preocupações incluem preços altos de
licenças, dificuldades em obter licenças e uso indevido de SEPs para travar a
concorrência. A proposta da Comissão Europeia inclui uma série de medidas para
abordar as preocupações relacionadas ao uso de SEPs. Avaliamos a proposta da
Comissão Europeia de forma positiva, mas também identificamos algumas áreas que
podem ser melhoradas e estamos disponíveis para vos ouvir sobre o assunto em
questão. A proposta pode ser melhorada e contamos com a vossa ajuda para
sugestões.

![](partido-l.png)
**LIVRE:**
O controlo corporativo de patentes limita a inovação e impede a liberdade de
expressão. [in 'Programa Eleitoral Europeias LIVRE 2024', capítulo Q. Soberania
Digital ponto 6.'Construir bens digitais comuns' (pg. 103)] Não temos ainda uma
posição pública sobre este assunto em específico, mas levando em conta as
nossas posições que se relacionam com o tema, nós no LIVRE partilhamos das
críticas e preocupações da comunidade de Software Livre. As Normas não devem
obrigar ao uso de soluções sob patente. As Organizações de Desenvolvimento de
Normas (SDOs) devem garantir o acesso aberto às normas publicadas e a
capacidade das mesmas serem cumpridas sem recurso a soluções patenteadas. Este
dossier das SEP é um exemplo claro de como um ecossistema baseado em licenças
abertas é fundamental para garantir a igualdade de oportunidades.


## Criptografia de Ponta-a-Ponta (E2EE)

**Os recorrentes ataques à Criptografia de Ponta-a-Ponta têm sido travados, mas
a questão está longe de estar resolvida, com várias propostas actualmente em
cima da mesa que tentam negar o direito à privacidade das comunicações em
situações específicas. Que considerações têm a fazer sobre esta temática?**

![](partido-nc.png)
**Nós, Cidadãos!**: Sem resposta.
<div style='clear: both; margin-bottom: 20px;'></div>

![](partido-cdu.png)
**CDU - Coligação Democrática Unitária:**
Como enunciado no actual "Compromisso do PCP para as Eleições ao Parlamento
Europeu", o PCP "recusa (...) prácticas de censura, vigilância e violação da
reserva da vida privada dos cidadãos", considerando que são "atentatórias de
direitos, liberdades e garantias". É neste âmbito que se enquadram as
iniciativas que visam garantir a possibilidade de escuta de comunicações
cifradas. Por esses motivos, já no mandato anterior, o PCP teve oportunidade de
se pronunciar contra estas directivas.

![](partido-be.png)
**Bloco de Esquerda:**
Partilhamos a preocupação com as repetidas investidas contra a encriptação
E2EE, que irão certamente continuar e que merecerão a nossa convicta oposição.
Não aceitamos que o direito à privacidade seja hipotecado em nome de ameaças
difusas, como a segurança das crianças online, que é um problema real mas que
implica abordagens muito diferentes das propostas que mencionam, e não
aceitamos que princípios como o da inviolabilidade da correspondência possam
ser colocados em causa. Sempre nos posicionámos do lado da privacidade dos
cidadãos, mesmo quando se reivindica prioridades de segurança nacional e assim
continuaremos.

![](partido-vp.png)
**Volt Portugal**:
O Volt pretende promover a privacidade e a segurança dos dados, sendo que
defendemos a proteção da criptografia de ponta-a-ponta como um direito
fundamental à privacidade das comunicações. A criptografia de ponta-a-ponta
desempenha um papel crucial na proteção dos dados pessoais e na garantia da
confidencialidade das comunicações, sendo essencial para a segurança
cibernética e a proteção dos direitos individuais dos cidadãos. Assim,
consideramos que devem ser continuados e redobrados os esforços para promover a
Criptografia de Ponta-a-Ponta.

![](partido-ad.png)
**AD - Aliança Democrática**:
Consideramos sempre que os governos acedam aos dados privados das pessoas, esse
acesso deve ser direcionado, proporcional e sujeito a supervisão. A proposta
original da Comissão Europeia, o órgão executivo da UE, permitiria às
autoridades da UE obrigar os serviços online a analisar todos os dados dos
utilizadores e compará-los com bases de dados policiais. O objetivo declarado
seria o de procurar crimes contra crianças, incluindo imagens de abuso sexual
infantil. A proposta da UE chegou mesmo a sugerir denunciar pessoas à polícia
como potenciais abusadores de crianças. Aliança Democrática considera que todo
o ser humano deve ter o direito a ter uma conversa privada. Isto é verdade no
mundo online, e não devemos abdicar desses direitos no mundo digital. No
entanto reforçamos mais uma vez quanto dito, o acesso deve ser direcionado,
proporcional e sujeito a supervisão. É vital que as comunicações permaneçam
protegidas no futuro para a segurança dos nossos cidadãos, sociedades,
economias e mercado único digital da UE.

![](partido-l.png)
**LIVRE:**
Não pode ser colocada em causa o direito à privacidade. O acesso indiscriminado
a este tipo de informação é desproporcionado e ilegal. Medidas como o "Chat
Control" não são aceitáveis em democracia. No entender do LIVRE, a encriptação
ponta a ponta é equiparável a um direito civil. Qualquer iniciativa que resulte
numa quebra definitiva desta encriptação ou no acesso tipo 'client side
scanning' deve ser liminarmente rejeitada. Do ponto de vista legal, isto coloca
o ónus da prova de forma invertida e isto não é admissível, tal como não o é
noutros contextos. [in 'Debate com os Partidos Políticos sobre "O Futuro da
Regulação da Internet na Europa"' em <https://www.youtube.com/watch?v=pdFwEm8EU_s>]


## Dados Abertos, Acesso Aberto e Recursos Educacionais Abertos

**O uso de Dados Abertos e o recurso ao Acesso Aberto tem sido, ainda que de
forma tímida e pouco coordenada, reconhecido como uma estratégia de vital
importância. A abertura da educação foi declarado um item importante na agenda
política Europeia, contudo ainda pouco se tem feito quanto ao tema. Que passos
consideram importantes serem dados nestas matérias?**

![](partido-nc.png)
**Nós, Cidadãos!**: Sem resposta.
<div style='clear: both; margin-bottom: 20px;'></div>


![](partido-cdu.png)
**CDU - Coligação Democrática Unitária:**
No âmbito da livre partilha do conhecimento, que o PCP defende, entra,
naturalmente, o conceito de dados abertos. Aliás, foi uma iniciativa
legislativa do PCP que deu origem ao Regulamento Nacional de Interoperabilidade
Digital: a Lei 36 de 2011, que estabelece a adopção de normas abertas para a
informação em suporte digital na Administração Pública, promovendo a liberdade
tecnológica dos cidadãos e organizações e a interoperabilidade dos sistemas
informáticos do Estado. As instituições públicas têm responsabilidades
acrescidas na garantia de acesso aos dados de forma aberta e utilizando normas
abertas. Não é admissível obrigar os cidadãos a usarem determinada marca ou
produto para acederem a certos serviços públicos, sejam eles a mera consulta de
dados, o preenchimento de impressos, ou o acesso a comunicações. Fundamental
para a livre partilha de conhecimento é, obviamente, a existência de recursos
educacionais públicos. Por isso, o PCP considera que todo o conhecimento
produzido em instituições públicas ou financiadas com dinheiros públicos deve
estar disponível de forma pública e aberta.

![](partido-be.png)
**Bloco de Esquerda:**
Os dados abertos, embora estejam ainda longe de cumprir a sua promessa de
envolvimento democrático da sociedade civil nos processos de decisão e
compreensão da sociedade, são incontornáveis como bandeira de uma sociedade
democrática e aberta. Defendemos a expansão do seu âmbito, nomeadamente através
da imposição de políticas obrigatórias de dados abertos dentro das instituições
públicas segundo o princípio "open by default". O acesso aberto é também uma
premissa necessária de uma política científica consequente, e também nos
preocupa a primazia dos journals académicos predatórios, com exemplos extremos
como o da Elsevier. Os recursos educacionais abertos deverão ser parte
integrante de uma política educacional europeia que coloque a aprendizagem e a
partilha de conhecimento em primeiro lugar. Hoje em dia resulta numa perversão
de todo o processo educativo, submetendo-o a lógicas que não têm o bem-estar
dos cidadãos como preocupação.


![](partido-vp.png)
**Volt Portugal**:
Para abordar as questões de Dados Abertos, Acesso Aberto e Recursos
Educacionais Abertos, o Volt propõe uma série de medidas importantes:

* Defesa da disponibilidade e abertura de dados públicos, considerando os dados
  abertos como um bem público, promovendo a partilha de dados públicos sob uma
  licença aberta e num formato aberto. Especificamente, o Volt insiste no
  caráter de código aberto da pesquisa financiada publicamente.
* Garantia da proteção de dados privados e assegurar que a publicação de dados
  públicos não comprometa a privacidade. O Volt defende a proteção da
  privacidade dos cidadãos em relação à possibilidade de agregação de grandes
  quantidades de dados coletados por entidades governamentais e privadas.
* Promoção da transparência governamental, incentivando a disponibilidade
  regular de dados governamentais, prioridades e descobertas para os cidadãos,
  de forma compreensível. Isso inclui a partilha de dados e serviços entre
  administrações para promover economias de escala e maior eficiência.
* Enfatizar os efeitos positivos potenciais dos dados abertos no conhecimento,
  no envolvimento dos cidadãos e na inovação.
* Assegurar que todos os Estados-Membros da UE e as suas autoridades locais
  cumpram a Carta de Dados Abertos, garantindo que os dados sejam abertos por
  padrão, publicados de forma oportuna e compreensível, acessíveis e
  utilizáveis, comparáveis e interoperáveis, e usados para melhorar a
  governança e o envolvimento dos cidadãos, bem como para o desenvolvimento
  inclusivo e a inovação.

![](partido-ad.png)
**AD - Aliança Democrática**:
Sem resposta.
<div style='clear: both; margin-bottom: 20px;'></div>

![](partido-l.png)
**LIVRE:**
No 'Programa Eleitoral Europeias LIVRE 2024', capítulo Q. Soberania Digital
ponto 5. 'Implementar a Rede de Dados Europeia autónoma' (pg. 103) pode-se ler:
"A atual estrutura da Internet é demasiado centralizada. Grande parte da nuvem
que armazena e gere os nossos dados é de propriedade privada e estruturada de
uma forma que permite o aproveitamento comercial de informação pessoal, bem
como vigilância estatal não declarada. Propomos uma nova Rede Autónoma de Dados
Europeia (RADE): uma rede de dispositivos descentralizada, anónima e encriptada
que proteja os nossos dados e impeça a vigilância massiva das pessoas. Todos os
serviços de interesse público devem ser baseados na RADE e todas as pessoas
devem receber uma Identidade Cidadã Digital, que lhes permita aceder a sites do
governo de forma certificada e participar em fóruns públicos com a opção de
salvaguarda da identidade para o exterior." No ponto 6.'Construir bens digitais
comuns' (pg. 103) pode-se ler também: O controlo corporativo de patentes e
direitos de autor limita a inovação e impede a liberdade de expressão.
Construiremos os bens digitais comuns restringindo o poder dos direitos de
autor. Propomos:
* que todo o código desenvolvido com dinheiro público fique no domínio público;
* expandir a cláusula de "Uso Justo" em todas as leis de direitos de autor;
* reverter o ónus da prova para que os bens sejam considerados bens digitais
  comuns, excepto se se provar estarem protegidos por direitos de autor;
* rever a Diretiva de Direitos de Autor da UE para reequilibrar os direitos dos
  utilizadores, criadores e inovadores.


## Soberania Digital

**Na constante transformação digital que vivemos, o Software Livre tornou-se
uma fundação crítica para a inovação e criatividade, servindo também como
elemento vital para a soberania digital Europeia. O seu impacto económico tem
sido estudado e evidenciado, além de promover uma sociedade digital mais
inclusiva, transparente e resiliente. Qual é a vossa visão sobre a importância
da soberania digital, e o que fazer para atingi-la?**

![](partido-nc.png)
**Nós, Cidadãos!**: Sem resposta.
<div style='clear: both; margin-bottom: 20px;'></div>


![](partido-cdu.png)
**CDU - Coligação Democrática Unitária:**
Dois dos pilares da política alternativa patriótica e de esquerda que o PCP
defende, é a "promoção e desenvolvimento da produção e riqueza nacionais (...)"
e "(...) serviços públicos ao serviço do país, capazes de garantir o direito à
saúde, à educação, à protecção social dos portugueses".

Por outro lado, o PCP equipara a importância estratégica da Infraestrutura
Digital com a dos "serviços públicos essenciais [como a] saúde, [o] ensino
e estruturas públicas de I&D, Energia, Transportes". Por isso, tem defendido,
também para estes sectores, "a aplicação dos Fundos (...) de forma soberana".

Para garantir a nossa soberania, também no sector digital, será essencial investir numa forte
infraestrutura digital pública, dotar o Estado de capacidade para responder às
exigências digitais de todo o Sector Público e desenvolver a produção nacional
igualmente nesta área. Assim, o PCP defende "a propriedade e controlo públicos
das principais infraestruturas de armazenamento, tratamento, circulação e
difusão de dados".

Sublinha-se a posição de longa data do PCP de aposta no
desenvolvimento do Software Livre em Portugal, como factor para esse
desenvolvimento e de defesa da nossa soberania. Exemplo disso são as resoluções
66 de 2004 e 202 de 2007.

O PCP também rejeita "processos de concentração capitalista e mecanismos (...)
que visam a uniformização cultural, o impedimento da livre partilha de
conteúdos, a exploração pelas grandes multinacionais dos direitos de autor, a
limitação da justa remuneração dos criadores e a censura digital" como a
directiva dos direitos de autor no mercado único digital da UE, citações que
constam do Compromisso do PCP para as Eleições ao Parlamento Europeu 2024.


![](partido-be.png)
**Bloco de Esquerda:**
A soberania digital é um ponto estratégico fundamental para qualquer estado
moderno. Opomo-nos ao outsourcing e à privatização da infra-estrutura
tecnológica, tanto no hardware como no software. Estamos convictos de que é
necessário um esforço de raiz para reverter os processos de privatização e
alienação dessa infra-estrutura, e que para tal esforço o software livre é um
ponto essencial. Também acreditamos que uma política pública assente no
princípio da soberania digital terá o efeito de potenciar indústrias
locais/nacionais e contestar o domínio atual da indústria norte-americana,
presente por exemplo nos últimos desenvolvimentos sobre o uso de software
Microsoft dentro da Comissão Europeia e os riscos que comporta.


![](partido-vp.png)
**Volt Portugal**:
A visão do Volt sobre a soberania digital e o papel do Software Livre é clara e
abrangente. O partido reconhece a importância da proteção da segurança e
resiliência da infraestrutura digital da UE, promovendo um ecossistema de
Software Livre e de código aberto (FLOSS) que impulsiona a inovação. O Volt
propõe as seguintes medidas, que visam fortalecer a segurança, a transparência
e a independência digital da UE, promovendo a inovação e a acessibilidade no
setor de tecnologia.

* Incentivar a substituição de software proprietário por soluções de código
  aberto na administração pública para criar uma estrutura transparente que
  reduza o risco de violações de dados, uso não autorizado de dados e
  dependência de fornecedores proprietários.
* Ativamente pressionar pela introdução de uma diretiva que obrigue os serviços
  e instituições da UE a migrar para software FLOSS.
* Criar uma plataforma para software FLOSS na administração pública e exigir
  que os funcionários públicos avaliem as soluções disponíveis na plataforma em
  termos de scope e custo antes de optar por software proprietário.
* Designar os editores como prestadores de serviços em concursos públicos
  baseados em FLOSS.
* Recomendar a adoção a nível nacional e sensibilizar sobre as vantagens do
  software FLOSS em comparação com soluções proprietárias.


![](partido-ad.png)
**AD - Aliança Democrática**:
A UE enfrenta uma série de desafios para alcançar a soberania digital,
incluindo:
* Dependência de tecnologias e serviços de países terceiros, como os Estados
  Unidos e a China;
* Ameaças cibernéticas crescentes, como ataques de ransomware e espionagem;
* Lacunas nas leis e regulamentos em torno de áreas como inteligência artificial
  e dados.

A UE também tem várias oportunidades para alcançar a soberania digital,
incluindo:
* Investimento em pesquisa e desenvolvimento em áreas como infraestrutura
  digital, segurança cibernética e inteligência artificial;
* Desenvolvimento de padrões e normas europeias para tecnologias digitais;
* Cooperação com outros países e organizações internacionais para promover a
  soberania digital.

A Aliança Democrática apoia:
* Aumento do investimento em pesquisa e desenvolvimento em áreas prioritárias;
* Desenvolver padrões e normas europeias para tecnologias digitais;
* Reforçar a segurança cibernética da EU;
* Promover a educação e as habilidades digitais entre os cidadãos da EU;
* Cooperar com outros países e organizações internacionais para promover a
  soberania digital.

Sublinhamos a necessidade de garantir que a soberania digital não seja usada
para criar barreiras comerciais ou discriminar outros países.


![](partido-l.png)
**LIVRE:**
Acreditamos numa Europa tecnologicamente soberana, onde os europeus tomam decisões sobre os seus dados, plataformas e inovação. Os nossos dados devem pertencer-nos, as nossas conversas privadas devem permanecer privadas e as inovações financiadas publicamente devem pertencer ao público. Pretendemos colocar o poder da tecnologia ao serviço das pessoas. [in 'Programa Eleitoral Europeias LIVRE 2024' - Q. Soberania Digital (pg. 101)]

O apoio do LIVRE a campanhas como a "Public Money? Public Code!" é representativo da nossa posição em relação às responsabilidades que vêm acopladas ao uso de dinheiros públicos.
E para além de palavras, o LIVRE age em conformidade com o que defende. A Forma de fazer política É política: a nossa plataforma interna de trabalho político colaborativo [Ponto LIVRE] foi criada e desenvolvida por Membros e Apoiantes com recurso exclusivo a soluções de software livre.

Defendemos a transição progressiva para software livre e de código aberto em todos os níveis das instituições da UE e em instituições financiadas com recursos públicos, desenvolvidos e mantidos por equipas internalizadas nos serviços da UE quando se trate de aplicações específicas e de uso não-universal. Queremos estruturar todos os registos que estão disponíveis ao público num banco de dados online aberto. A tecnologia pode e deve ser um veículo de transparência. [in 'Programa Eleitoral Europeias LIVRE 2024' - Q. Soberania Digital - 4. Trabalhar pela governação livre e normas de acesso aberto (pg. 102)]

## Ecologia e Sustentabilidade

**Abordagens técnicas e culturais abertas são essenciais para apoiar tomadas de
decisão auditáveis, e criar a capacidade de localização e personalização,
providenciado novas oportunidades para a participação, garantindo confiança e
transparência. O Software Livre, cujo modelo aberto tem sido usado para aplicar
essas abordagens, contribui para uma transição ecológica e sustentabilidade
digital na União Europeia. Que políticas pensam promover para encorajar e
potenciar esta contribuição?**


![](partido-nc.png)
**Nós, Cidadãos!**: Sem resposta.
<div style='clear: both; margin-bottom: 20px;'></div>

![](partido-cdu.png)
**CDU - Coligação Democrática Unitária:**
Das medidas que defendemos para assegurar uma transição justa destacamos:
- Promoção da eficiência energética e de um maior aprovisionamento energético
  oriundo de fontes renováveis, privilegiando os recursos endógenos de cada
  país, a par da racionalização dos consumos, no quadro do respeito pelo
  direito soberano de cada país de definir a sua política energética e o seu
  combinado de fontes de energia.
- Reforço do investimento na investigação e no desenvolvimento tecnológico,
  visando uma indústria cada vez menos poluente e uma maior sustentabilidade
  dos meios urbanos, onde se concentra tendencialmente a grande maioria da
  população.

![](partido-be.png)
**Bloco de Esquerda:**
A sustentabilidade passa pela partilha e aproveitamento das soluções
tecnológicas, pelo que o software livre é essencial numa agenda que valorize a
sustentabilidade. Apoiamos a iniciativa Public Money Public Code, para
assegurar que os frutos do investimento público são efectivamente devolvidos à
sociedade e potenciar um ciclo virtuoso de crescimento sustentável. O profundo
desperdício de dinheiros públicos em licenças proprietárias, como é exemplo o
domínio da Microsoft nas escolas públicas; esses fundos podiam ser muito melhor
aplicados na resiliência e sustentabilidade das instituições. A mobilização da
comunidade maker no desenvolvimento de soluções durante a pandemia foi um ótima
demonstração de como um modelo de partilha livre e aberta do conhecimento e das
ferramentas pode ser uma medida efetiva para abordar os dilemas ecológicos que
nos esperam nos próximos anos.

![](partido-vp.png)
**Volt Portugal**:
A sustentabilidade é o tema prioritário do Volt em toda a Europa. Para promover
a ecologia e a sustentabilidade, o Volt propõe uma série de políticas e medidas
que visam contribuir para uma transição verde e uma agricultura mais
sustentável na União Europeia, muitas delas potenciadas por Software Livre e a
sua legislação. Sabemos que a promoção do Software Livre contribui para a
redução da pegada de carbono, uma vez que o desenvolvimento e utilização de
software de código aberto tende a ser mais eficiente em termos de recursos do
que o software proprietário. Menos recursos são necessários para produzir e
manter software livre, o que pode resultar em menor consumo de energia e menor
impacto ambiental. Por outro lado, o modelo aberto do Software Livre promove a
transparência e a colaboração, permitindo que mais pessoas contribuam para o
desenvolvimento de soluções sustentáveis. Isso pode levar a inovações mais
rápidas e eficazes no campo da sustentabilidade ambiental, que é por nós
fortemente defendida. O Software Livre também proporciona acesso aberto ao
código fonte, permitindo a personalização e adaptação de soluções de software
para atender às necessidades específicas de projetos de sustentabilidade. Isso
pode facilitar a implementação de novas tecnologias verdes e a criação de
soluções personalizadas para desafios ambientais. Ao mesmo tempo, o Software
Livre promove a reutilização e a partilha de recursos, o que está alinhado com
os princípios da economia circular que o Volt defende. Ao utilizar e contribuir
para projetos de Software Livre, as organizações podem reduzir o desperdício e
maximizar a eficiência dos recursos. Em resumo, o Volt defende que o Software
Livre pode desempenhar um papel fundamental na promoção da sustentabilidade
ambiental, oferecendo soluções tecnológicas mais eficientes, transparentes e
adaptáveis para enfrentar os desafios ambientais atuais, e é também por isso
que o Volt o defende em todo o continente.

![](partido-ad.png)
**AD - Aliança Democrática**:
Sem resposta.
<div style='clear: both; margin-bottom: 20px;'></div>

![](partido-l.png)
**LIVRE:**
No 'Programa Eleitoral Europeias LIVRE 2024' (pg. 103) pode-se ler: "O programa
Horizonte Europa investe anualmente milhares de milhões de euros em
investigação e inovação. No entanto, cidadãos, cidadãs e pessoas residentes não
têm como dar a sua opinião direta quanto à alocação desses fundos e o controlo
sobre os produtos ou as patentes que resultam desse incentivo económico
continua a ser a exceção. Iremos dar mais voz às pessoas sobre este programa:

* atribuindo mais recursos aos projetos cooperativos e às organizações da
  sociedade civil de cariz social;
* propondo uma linha piloto de financiamento que responda a prioridades
  identificadas pelos cidadãos, cidadãs e pessoas residentes;
* instituindo direitos de propriedade coletivos para os produtos resultantes do
  investimento público;
* incentivando as inovações aberta e colaborativa entre empresas, academia e
  sector público para desenvolver soluções de Inteligência Artificial (IA) que
  atendam às necessidades da sociedade e promovam o interesse público;
* acompanhando e avaliando o impacto social e económico da IA nas comunidades e
  desenvolver mecanismos de deteção de vieses, evitando a discriminação gerada
  pelo algorítmico e garantindo a representação equitativa de todas as pessoas;
* investindo na educação de profissionais do sector público, de forma a
  garantir as competências necessárias para desenvolver e utilizar os vários
  domínios tecnológicos de forma ética e responsável. Acreditamos que o
  dinheiro público deve gerar conhecimento público, propriedade pública e
  riqueza comum.

O mesmo princípio se aplica ao desenvolvimento da IA. A UE deve garantir o
escrutínio da regulação em vigor, bem como incentivar a atualização contínua da
mesma. Para além disso, rejeitamos a associação tendencial da definição de
inovação presente no programa Horizonte Europa e nas políticas industriais da
União Europeia com o desenvolvimento de bens e serviços para o mercado." [in
'Programa Eleitoral Europeias LIVRE 2024' - Q. Soberania Digital - 7.
Democratizar a investigação e a inovação (pg. 104)]

## Neutralidade da Internet

**Nos últimos anos, têm havido várias tentativas de convencer o legislador
Europeu a criar uma obrigação de pagamentos de grandes fornecedores de
conteúdos e aplicações (CAPs) aos fornecedores de serviços de Internet (ISPs).
Para além de outros problemas, uma proposta deste tipo tem como consequência a
destruição da neutralidade da Internet. No ano passado, os ISPs voltaram a
pressionar a Comissão Europeia, o que originou críticas de todos os sectores,
incluindo dos representantes dos reguladores Europeus para as comunicações
electrónicas, que emitiram um [relatório arrasador](https://www.berec.europa.eu/system/files/2023-05/BoR%20%2823%29%20131d%20Annex%20to%20Section%204.pdf).
A Comissão Europeia decidiu adiar a criação da proposta. Qual é a vossa posição
sobre a neutralidade da Internet?**

![](partido-nc.png)
**Nós, Cidadãos!**: Sem resposta.
<div style='clear: both; margin-bottom: 20px;'></div>

![](partido-cdu.png)
**CDU - Coligação Democrática Unitária:**
O PCP defende "a total transparência e neutralidade das redes". Tem
desenvolvido várias iniciativas sobre o tema, nomeadamente os Projetos de Lei
418/XI/2 e 103/XII, entre audições e debates sobre o tema. O "tratamento
igualitário de todo o tráfego", volta hoje a estar ameaçado pela voracidade
capitalista e pela subordinação da UE aos interesses das grandes
multinacionais. Por outro lado, para evitar a "censura de conteúdos por motivos
políticos", o PCP pugna pela "neutralidade da rede perante todo o tipo de
conteúdos". No domínio tecnológico, são vários os direitos ameaçados e, por
isso, o PCP procura a  "proibição da utilização de tecnologias que se dedicam a
recolher de forma invasiva dados pessoais (...) a vigilância permanente em
contexto laboral, a devassa automatizada da privacidade, a recolha de dados
biométricos".

![](partido-be.png)
**Bloco de Esquerda:**
Temos estado sempre na linha da frente a defender a neutralidade da Internet [a
Marisa estava mesmo por dentro disso] e a nossa posição não mudou. É
fundamental contestar as investidas das operadoras no sentido de limitar o
acesso livre e completo à internet, com recurso a tácticas enganadoras como o
zero-rating. Sabemos que o fim da neutralidade da Internet prenunciará o fim da
Internet livre e a sua redução a um conjunto de sites e redes sociais. Sabemos
que a Internet tem de continuar a ser das pessoas, e defender o seu livre
usufruto e exploração por parte da sociedade é um objectivo central para nós
numa política voltada para as pessoas.

![](partido-vp.png)
**Volt Portugal**:
O Volt defende a importância de garantir a neutralidade da Internet como um
princípio fundamental para preservar a igualdade de acesso, a liberdade de
expressão e a inovação online. O Volt apoia a manutenção de um ambiente online
aberto e equitativo, onde todos os dados e conteúdos são tratados de forma
igual, sem discriminação ou priorização com base em interesses comerciais. A
neutralidade da Internet é essencial para garantir que os utilizadores tenham
acesso livre e justo a todos os conteúdos e serviços online, sem interferência
indevida por parte dos fornecedores de serviços de Internet. Ao preservar a
neutralidade da Internet, o Volt visa promover um ambiente digital democrático,
inclusivo e transparente, onde a diversidade de opiniões e a livre circulação
de informações são protegidas.

![](partido-ad.png)
**AD - Aliança Democrática**:
Sem resposta.
<div style='clear: both; margin-bottom: 20px;'></div>

![](partido-l.png)
**LIVRE:**
Já em 2019 o LIVRE assumia a defesa intransigente da "Neutralidade da rede"
(Net Neutrality) através da aprovação em Congresso da Moção ['Por uma Internet
LIVRE'](https://partidolivre.pt/wp-content/uploads/2019/01/Por-uma-Internet-LIVRE-MocaoVIICongressoLIVRE.pdf),
onde se assumia ainda de forma clara a luta:

* Por uma forte oposição à censura de conteúdos por parte de governos ou
  empresas
* Pela defesa de um acesso universal à internet que impeça a exclusão de
  qualquer cidadão, independentemente da sua condição económica, e por uma
  promoção ativa da literacia digital
* Pela defesa do direito à privacidade online e ao direito ao esquecimento,
  devendo cada cidadão ter controle sobre os seus dados pessoais (direito à
  dissipação da informação, no sentido de permitir o esquecimento)
* Pelo direito a ter acesso, a fazer recolha sistemática de dados e a criar
  conteúdos diversos
* Pela defesa de uma web alicerçada em tecnologias estandardizadas com base em
  código aberto
* Pelo direito à liberdade de expressão e associação online
* Pela defesa e promoção do software de código aberto, em especial na esfera
  pública, seja nos serviços do estado ou em projectos com financiamento
  público
* Pelo direito à utilização livre de conteúdos em contexto de ensino
* Pelo direito à construção colaborativa de software e hardware
* Pelo direito à Liberdade e à Privacidade através da promoção da encriptação
  das comunicações online
* Pela promoção do "A Contract for the Web" e pela sua assinatura pelo Governo
  Português

No Programa Eleitoral para as Europeias 2024, pode-se ler: "Introduziremos
legislação que consagre o direito à Internet livre e sem censura. Para
concretizar o acesso universal à Internet, a legislação atribuirá aos
Estados-Membros a responsabilidade de desenvolver e expandir a sua
infraestrutura digital. Para garantir que todas as pessoas têm a capacidade de
navegar na Internet, colocaremos de novo em prática o "Programa de Aprendizagem
ao Longo da Vida" da UE para desenvolver a literacia digital e a capacidade
digital em toda a Europa. Garantiremos o cumprimento da neutralidade da rede de
acordo com o Regulamento 2015/2120, colocando nos fornecedores de serviço de
acesso à Internet em toda a UE o ónus de demonstrar que tratam toda a
transmissão de dados nas suas redes de forma igual no que concerne à
velocidade, latência e preço, independentemente da fonte, protocolo ou
aplicação a que os dados são destinados." [in 'Programa Eleitoral Europeias
LIVRE 2024' - Q. Soberania Digital - 1. Garantir o direito à Internet livre e
universal (pg. 101)]

## Comentários adicionais nas declarações dos partidos

![](partido-ad.png)
**AD - Aliança Democrática**:
O Código Aberto desempenha um papel crucial na economia digital, impulsionando
a inovação, a competitividade e a eficiência. A UE reconhece a importância de
promover um ecossistema de Código Aberto forte e vibrante na Europa. A UE visa
aumentar a adoção de Código Aberto em todos os setores, incluindo o setor
público, as empresas e a sociedade civil.

Ações que devem continuar a ser implementadas:
* Criar centros de informação para fornecerem apoio e orientação aos utilizadores;
* Reforço dos programas de financiamento para apoiar projetos de Código Aberto;

A Aliança Democrática reconhece a importância da diversidade e da inclusão no
ecossistema de Código Aberto, destacando, no entanto, a necessidade de garantir
a segurança e a confiabilidade das soluções de Código Aberto. Desta forma a
Aliança Democrática suportará uma abordagem global de forma que se promova o
Código Aberto.

<style>
article.article img {
float: left;
width: 60px;
margin: 0px;
margin-right: 10px;
}
</style>
