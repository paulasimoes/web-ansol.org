---
layout: article
title: A Festa do Software Livre regressa a Aveiro para comemorar os seus 20 anos
date: 2024-09-16
---

A ANSOL – Associação Nacional para o Software Livre, em conjunto com
diversas associações e comunidades ligadas ao Software Livre, organizam nos
próximos dias 11 e 12 de outubro (sexta e sábado) a "Festa do Software Livre
2024".

<!--more-->

<p style="text-align: center">
  <strong>11 e 12 de outubro de 2024 na Universidade de Aveiro</strong>
</p>
<p style="text-align: center">
  <a href="https://festa2024.softwarelivre.eu/">https://festa2024.softwarelivre.eu</a>
</p>


A Festa do Software Livre é um encontro comunitário onde, ao longo de 2 dias, decorrem **apresentações, painéis, oficinas, demonstrações e eventos sociais**, onde haverá oportunidades de experimentar, aprender e debater Software Livre e o seu impacto técnico e sócio-cultural.

É um evento de **entrada gratuita e aberto ao público em geral** independentemente dos seus conhecimentos e experiência técnica.

O evento decorrerá no **Departamento de Eletrónica, Telecomunicações e Informática da Universidade de Aveiro** (Campus Universitário de Santiago) entre as 8h30 e as 19h30.

A ANSOL oferece alojamento gratuito para estudantes, e pessoas em situação de desemprego, reformados e voluntários. Os detalhes desta oferta podem ser encontrar no site da Festa.

Vamos ter a presença de múltiplas comunidades e associações como:

- [ANSOL - Associação Nacional para o Software Livre](https://ansol.org/)
- [Associação Wikimedia Portugal](https://pt.wikimedia.org/)
- [Associação Inércia](https://inercia.pt/)
- [Associação D3 - Defesa dos Direitos Digitais](https://direitosdigitais.pt/)
- [Drupal Portugal](https://drupal.pt/)
- [ESOP - Associação de Empresas de Software Open Source Portuguesas](https://esop.pt/)
- [Flutter Portugal](https://flutter.pt/)
- [GLUA - Grupo Linux da Universidade de Aveiro](https://glua.ua.pt/)
- [Ubuntu-PT](https://ubuntu-pt.org/)


<!--
O evento conta com o patrocínio das seguintes entidades (_em atualização_):

- [Dropsolid](https://dropsolid.com/)
-->
