---
categories:
- assembleia geral
layout: article
title: Assembleia Geral Eleitoral 2024
date: 2024-01-16
---

Caros sócios,

Como Presidente da Assembleia Geral, cabe-me cumprir o dever de vos
convocar para a Assembleia Geral eleitoral de 2024.

A mesma terá lugar no "MILL - Makers In Little Lisbon", no dia 16 de
Março de 2024, com início às 15h00 da tarde, com a seguinte ordem de
trabalhos:

1. Apresentação e aprovação do relatório e contas de 2023
2. Apresentação da proposta de plano de actividades 2024 das listas;
3. Eleições dos Órgãos Sociais até à Assembleia Geral Eleitoral de 2026;
4. Revisão dos estatutos, tendo em vista a obtenção de estatuto de utilidade publica;
5. Apresentação e aprovação do plano estratégico para os próximos 6 anos;
6. Outros.

A apresentação das candidaturas deverá ser feita por carta, que deverá
dar entrada na sede social da Associação até dia 1 de Março de 2024.

Se à hora marcada não estiverem presentes, pelo menos, metade dos
associados a Assembleia Geral reunirá, em segunda convocatória, no mesmo
local e passados 30 minutos, com qualquer número de presenças.

- Localização no mapa: https://osm.org/go/b5crq85X0-?layers=C&way=98032128
- Coordenadas GPS: geo:41.14378,-8.61263?z=19
- Morada: Calçada do Moinho de Vento, 14B, 1150-236 Lisboa
- Ajuda adicional a encontrar o local em: https://centrolinux.pt/ondeestamos/

--
Diogo Miguel Constantino dos Santos, Presidente da Mesa da Assembleia Geral da ANSOL
