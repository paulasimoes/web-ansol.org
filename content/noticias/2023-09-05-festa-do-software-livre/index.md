---
layout: article
title: Festa do Software Livre 2023
date: 2023-09-05
---

A ANSOL – Associação Nacional para o Software Livre, em conjunto com diversas
associações e comunidades ligadas ao Software Livre, organizam nos próximos
dias 15, 16 e 17 de Setembro a "Festa do Software Livre 2023".

<!--more-->

<p style="text-align: center">
  <strong>15 a 17 de setembro de 2023 na Universidade de Aveiro</strong>
</p>
<p style="text-align: center">
  <a href="https://festa2023.softwarelivre.eu/">https://festa2023.softwarelivre.eu/</a>
</p>

A Festa do Software Livre é um encontro de várias comunidades onde, ao longo de 3
dias, irão decorrer **cerca de 100 apresentações, painéis e workshops**. É um
evento de entrada gratuita e aberto ao público em geral independentemente dos
seus conhecimentos, onde serão discutidas várias aplicações de Software Livre na
sociedade.

O evento decorrerá no Departamento de Eletrónica, Telecomunicações e
Informática da Universidade de Aveiro (Campus Universitário de Santiago) entre as
9h00 e as 19h00.

Para participar não é necessário ter quaisquer conhecimentos técnicos.
A ANSOL oferece alojamento gratuito para estudantes que participem no evento,
bastando fazer a reserva por email via contacto@ansol.org.

Vamos ter a presença das seguintes associações e comunidades:

- [Associação Wikimedia Portugal](https://pt.wikimedia.org)
- [Associação Inércia](https://inercia.pt)
- [Associação D3 - Defesa dos Direitos Digitais](https://direitosdigitais.pt)
- [CryptoCafe](https://cryptocafe.pt)
- [DevPT](https://devpt.co)
- [Drupal Portugal](https://drupal.pt)
- [ESOP - Associação de Empresas de Software Open Source Portuguesas](https://esop.pt)
- [Flutter Portugal](https://flutter.pt)
- [GLUA - Grupo Linux da Universidade de Aveiro](https://glua.ua.pt)
- [Ubuntu-pt](https://ubuntu-pt.org)

O evento conta com o patrocínio das seguintes entidades:

- [Banco Santander](https://www.santander.pt/)
- [Dropsolid](https://dropsolid.com/)
- [INFO-CARE](https://www.info-care.com.pt)
- [Luso Digital Assets](https://www.lusodigitalassets.com)
- [OnlyOffice](https://onlyoffice.com/)
- [PTServidor](https://ptservidor.pt)
- [Students for Liberty](https://studentsforliberty.org/)
- [Ubiwhere](https://www.ubiwhere.com/)
- [Unikraft](https://unikraft.org/)
