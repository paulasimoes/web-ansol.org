---
categories:
- distribuição/venda
- formação
metadata:
  email:
  - email_email: info@tintadigital.com
  servicos:
  - servicos_tid: 8
  - servicos_tid: 3
  site:
  - site_url: http://www.tintadigital.com/content/view/35/31/
    site_title: http://www.tintadigital.com/content/view/35/31/
    site_attributes: a:0:{}
  node_id: 51
layout: servicos
title: TintaDigital - Soluções em Tecnologias de Informação
created: 1334501648
date: 2012-04-15
aliases:
- "/node/51/"
- "/servicos/51/"
---
<p><strong>Servi&ccedil;os</strong></p>
<ul>
	<li>
		Defini&ccedil;&atilde;o e planeamento de solu&ccedil;&otilde;es;</li>
	<li>
		Instala&ccedil;&otilde;es;</li>
	<li>
		Actualiza&ccedil;&otilde;es (upgrades);</li>
	<li>
		Despistagem de problemas (troubleshooting);</li>
	<li>
		Migra&ccedil;&otilde;es;</li>
	<li>
		Optimiza&ccedil;&otilde;es;</li>
	<li>
		Integra&ccedil;&atilde;o de aplica&ccedil;&otilde;es (i.e. openLDAP + Apache);</li>
	<li>
		Administra&ccedil;&atilde;o de sistemas (on-site, remote, mista).</li>
</ul>
<p><strong>Aplica&ccedil;&otilde;es</strong></p>
<ul>
	<li>
		Bases de dados;</li>
	<li>
		ERP/CRM;</li>
	<li>
		C&oacute;pias de Seguran&ccedil;a (backups);</li>
	<li>
		Gest&atilde;o de Armazenamento;</li>
	<li>
		Clusters;</li>
	<li>
		Servidores de correio electr&oacute;nico;</li>
	<li>
		Servidores de Web / Proxies;</li>
	<li>
		Desktop;</li>
	<li>
		Gest&atilde;o e Monitoriza&ccedil;&atilde;o;</li>
	<li>
		Virtualiza&ccedil;&atilde;o;</li>
	<li>
		Reporting;</li>
</ul>
<p><strong>Networking</strong></p>
<ul>
	<li>
		Firewalls;</li>
	<li>
		Routers/Switches/...;</li>
	<li>
		Balanceadores de Carga.</li>
</ul>
<p><strong>Sistemas Operativos</strong></p>
<ul>
	<li>
		Linux (RedHat, Suse, Fedora, Gentoo, Debian);</li>
	<li>
		FreeBSD;</li>
	<li>
		Sun Solaris;</li>
	<li>
		Sun Java Desktop System.</li>
</ul>
<p><strong>Outros</strong></p>
<ul>
	<li>
		Scripting (Perl, PHP, Shell);</li>
	<li>
		Programa&ccedil;&atilde;o (C, C++, Java);</li>
	<li>
		Hardware (Intel, Sparc).</li>
</ul>
<p><strong>Tecnologia</strong></p>
<p>MySQL, Postgres, Firebird, Fyracle, Oracle, Compiere, SugarCRM, Veritas NetBackup, Veritas BackupExec, Legato Networker, Veritas FileSystem, Veritas Volume Manager, Solaris Logical Volume Manager (Solstice DiskSuite), Veritas Cluster Server, Sun Cluster, Exim, Postfix, Courier, Sendmail, Majordomo, phpGroupWare, Sun Java System Messaging Server, Sun Java System Calendar System, Sun Java System Portal Server, HylaFax, smsd, ISC Bind, ProtFTPD, WU-FTPD, Apache, ht://Dig, Squid, Sun ONE Web Server, Sun ONE Proxy Server, openLDAP, Sun Java System Directory Server, Sun Java System Directory Proxy Server, Sun ONE Certificate Server, K Desktop Environment, Gnome, OpenOffice.org, X11, Nagios, Big Brother, Sun Management Center, Xen, VMware GSX Server, VMware ESX Server, VMware Workstation, WebTrends Entreprise Reporting Server, Control/M Enterprise Manager, TeamQuest Performance Suite, BMC Patrol, ISC DHCP, Samba, OpenSSH, MRTG, INN, Outras aplica&ccedil;&otilde;es OpenSource, LDAP, HTTP, SMTP, FTP, POP3, IMAP4, XML, Firewalls (netfilter/iptables, ipf), Routers Cisco, Switches Cisco, IOS Cisco, Alteon ACE Director Switches, Alteon WEB OS, Solaris, Sun Java Desktop System, RedHat Linux, Suse Enterprise Linux, FreeBSD, Fedora, Gentoo, Debian, Perl, PHP, Bourne Shell, Bash, C, C++, Java, Intel, Sparc.</p>
