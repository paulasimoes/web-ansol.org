---
layout: article
title: Ideias para a legislatura 2024-2028
date: 2024-01-12
image:
  caption: |
    Palácio de São Bento,
    por [Manuel Menal](https://www.flickr.com/photos/mmenal/9307363528),
    sob a licença [CC BY-SA 2.0](https://creativecommons.org/licenses/by-sa/2.0/)
---

Em março decorrerão as próximas eleições legislativas. A ANSOL apresenta, à
semelhança [do que fez em 2022][2022], ideias para a próxima Legislatura.

<!--more-->

## 1. Renovação do Regulamento Nacional de Interoperabilidade Digital

O RNID - Regulamento Nacional de Interoperabilidade Digital - regula a Lei das
Normas Abertas. A sua revisão devia ter acontecido, por lei, até dia 19 de
outubro de 2022. **A revisão do RNID deve ser feita, não só para fazer a
necessária atualização tecnológica, como também para munir a própria Lei dos
mecanismos necessários para que ela se faça cumprir.** Por exemplo, podem ser
introduzidos mecanismos de queixa com tempos de resposta previstos e ou
medidas sancionatórias ou incentivos ao cumprimento.


## 2. Dinheiro Público? Código Público!

A Administração Pública é uma grande produtora e consumidora de software. Para
maximizar o valor do software produzido para o setor público, este deve ser
publicado com uma licença de Software Livre. Se se trata de dinheiro público, o
código também deve ser público. Ficam aqui alguns exemplos de avanços que se
podem fazer nesta área:

À semelhança do que se fez noutros países ([França][fr], [Alemanha][de],
[EUA][us]), **deveria ser criada uma plataforma nacional para incentivar a
publicação e partilha de Software Livre criado e/ou usado pelas entidades da
administração pública.**

Também seria útil **democratizar o acesso à informação retida na plataforma de
pareceres prévios**, bem como o reforço ao Código dos Contratos Públicos no
sentido de **impedir a prática ilegal de procuração de licenças de software de
marcas ou fabricantes específicos**, através de melhor fiscalização, mais rigor
na fase dos pareceres prévios, e melhores mecanismos de denúncia.


## 3. FediGov – Comunicação federada nas instituições públicas

As instituições públicas usam maioritariamente plataformas proprietárias como
Twitter, Facebook, Instagram e YouTube. As empresas donas destas plataformas
controlam quem as pode utilizar e como podem ser utilizadas. Ao promover e
utilizar estas serviços, as instituições públicas acabam também por publicitar
e incentivar a sua utilização.

Queremos convencer as instituições públicas a repensar o seu uso das redes
sociais. **Isto é possível com a transição gradual para soluções federadas de
Software Livre, como o Mastodon ou Peertube. Para facilitar a sua adopção,
sugerimos que se disponibilize estes serviços a todos municípios.**


## 4. Internet Aberta – Definição do Ponto de Terminação de Rede

Tal como escolhemos livremente que telemóvel ou computador comprar, deveríamos
poder escolher livremente quais equipamentos de rede estão instalados nas
nossas casas. Mas alguns prestadores de serviço de acesso à Internet
desrespeitam este princípio, ao imporem aos seus clientes que equipamentos usar
na ligação à Internet ou ao discriminarem os detentores de equipamentos
alternativos.

Segundo o Código Europeu das Comunicações Eletrónicas, a fronteira entre o
domínio do fornecedor de acesso à Internet e o domínio do consumidor é definida
pelo Ponto de Terminação de Rede (PTR). Cabe a cada Autoridade Reguladora
Nacional – a ANACOM, em Portugal – definir a localização exacta desta
fronteira. **A definição do PTR deve ser feita no chamado Ponto A, garantindo
que os roteadores e modems façam parte do domínio do consumidor.**

De acordo com [as orientações do Organismo de Reguladores Europeus das
Comunicações Eletrónicas][berec], definir o PTR no Ponto A aumentará a inovação e
competição no mercado de equipamentos de telecomunicações e garantirá que o
tráfego local (por exemplo, na utilização de impressoras, digitalizadores, ou
ecrãs em rede, e em sistemas de domótica) se mantém privado e não faz parte da
rede pública.


## 5. O Direito Universal para instalar qualquer software em qualquer dispositivo

Com a digitalização contínua de serviços e infraestruturas, há um número
cada vez maior de **dispositivos eletrónicos ligados à Internet** - seja em
ambientes privados, públicos, ou empresariais. Muitos desses dispositivos
precisam de mais energia e recursos naturais para serem produzidos do que a
energia que consomem durante toda a sua vida útil, e **grande parte vai parar ao
lixo só porque o software deixa de ser atualizado**.

Considerando que o acesso livre ao hardware e ao software determina por quanto
tempo ou com que frequência um dispositivo pode ser usado ou reutilizado, e que
o aumento da longevidade e a reutilização dos nossos dispositivos são
inevitáveis para uma sociedade digital mais sustentável, na [carta aberta
assinada por mais de 3000 indivíduos e por mais de 147
organizações][openletter], exigimos que:

- os utilizadores tenham o direito de escolher livremente os sistemas
  operativos e software executados nos seus dispositivos;
- os utilizadores tenham o direito de escolher livremente entre os fornecedores
  de serviços para ligar os seus dispositivos;
- os dispositivos sejam interoperáveis e compatíveis com normas abertas;
- o código-fonte de controladores, ferramentas e interfaces sejam publicados
  sob licença livre.


## 6. Incentivo à criação e manutenção de Software Livre

Software Livre é a fundação de quase toda a infraestrutura digital, quer a
nível empresarial, quer a nível da administração pública. A maioria dos
projectos de Software Livre são criados e mantidos de forma voluntária,
dependendo de donativos.

Há poucas empresas que conseguem apoiar estes projectos contratando pessoal
para trabalhar a tempo inteiro, algo que não é viável para empresas mais pequenas
ou fora da área tecnológica. Outra forma de contribuírem é através de
donativos, mas há entraves fiscais por não serem considerados custos de
exercício. **Sugerimos que se revejam os estatutos de benefícios fiscais para
garantir que donativos para projectos de Software Livre sejam dedutíveis ao
lucro tributável**.

À semelhança do que se faz noutros países ([Sovereign Tech Fund][stf],
[NLNet][nlnet]), sugerimos **a criação de um programa de bolsas para o
desenvolvimento de projectos de Software Livre**.

[fr]: https://code.gouv.fr/fr/
[de]: https://opencode.de/en
[us]: https://code.gov/
[berec]: https://www.berec.europa.eu/en/document-categories/berec/regulatory-best-practices/guidelines/berec-guidelines-on-common-approaches-to-the-identification-of-the-network-termination-point-in-different-network-topologies
[openletter]: https://fsfe.org/activities/upcyclingandroid/openletter.pt.html
[stf]: https://www.sovereigntechfund.de/
[nlnet]: https://nlnet.nl/

[2022]: /noticias/2022-01-03-dez-ideias-para-a-proxima-legislatura/
