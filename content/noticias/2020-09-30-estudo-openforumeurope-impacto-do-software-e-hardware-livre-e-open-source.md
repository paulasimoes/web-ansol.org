---
categories: []
metadata:
  image:
  - image_fid: 59
    image_alt: ''
    image_title: ''
    image_width: 1024
    image_height: 512
  node_id: 753
layout: article
title: 'Estudo @OpenForumEurope: Impacto do Software e Hardware Livre e Open Source'
created: 1601486244
date: 2020-09-30
aliases:
- "/article/753/"
- "/node/753/"
---
<p>A OpenForum Europe (OFE) e a&nbsp;Fraunhofer ISI estão a conduzir um estudo para a Comissão Europeia sobre o impacto do software e hardware open source na inovação, competitividade, e independência tecnológica. Neste contexto, aquelas entidades lançaram um questionário a ser respondido por todos aqueles que usam, trabalham, desenvolvem hardware ou software open source</p><blockquote><p><em>Are you involved in or responsible for open source projects in your organization? Do you develop Open Source solutions yourself? Maybe you are a part of a scientific team implementing Open Source Hardware into their research? Or using Open Source components in your other projects?&nbsp;We invite all types of stakeholders using or developing Open Source, such as SMEs, large companies, non-profits, individual developers and many others, to fill the survey and share their experience."</em></p></blockquote><div>&nbsp;</div><div>O questionário pode ser <a href="https://inno.limequery.com/436575" target="_blank">respondido até 23 de outubro neste site</a>. <strong>O estudo servirá de base para desenvolver um conjunto de recomendações ao legislador europeu sobre hardware e software open source, daí a importância do questionário ser preenchido pelo maior número de pessoas.</strong></div><div>&nbsp;</div><div>No dia 5 de novembro, serão divulgados alguns resultados no evento Europe's Digital Decade Empowered by Open Source, organizado pela (OFE) com a seguinte ordem de trabalhos (mais informação a anunciar brevemente):</div><div>&nbsp;</div><div><ul><li>a preview of the economic impact of Open Source;</li><li>Open Source policy trends around the world;</li><li>the latest initiatives of the European Commission and Member States;</li><li>discussions on findings and actions with experts from academia, business and policy.</li></ul></div>
